import path_provider;
import UIKit
import Flutter
import GoogleMobileAds

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    GADMobileAds.sharedInstance().start(completionHandler: nil)


    registerOtherPlugins()

    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
  func registerOtherPlugins() {
          if !hasPlugin("io.flutter.plugins.pathprovider") {
              FLTPathProviderPlugin
                .register(with: registrar(forPlugin: "io.flutter.plugins.pathprovider")! )
          }
   }

}
