package co.sageiptv.mobile

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import io.flutter.view.FlutterMain
import io.flutter.plugins.pathprovider.PathProviderPlugin
import com.tekartik.sqflite.SqflitePlugin
import io.flutter.plugin.common.PluginRegistry

class BaseApp : MultiDexApplication(), PluginRegistry.PluginRegistrantCallback {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
        FlutterMain.startInitialization(this)
    }

    override fun registerWith(registry: PluginRegistry?) {
        if (!registry!!.hasPlugin("io.flutter.plugins.pathprovider")) {
            PathProviderPlugin.registerWith(registry.registrarFor("io.flutter.plugins.pathprovider"))
        }

        if (!registry!!.hasPlugin("com.tekartik.sqflite")) {
            SqflitePlugin.registerWith(registry.registrarFor("com.tekartik.sqflite"))
        }
    }
}