import 'package:mobile/local_database_guest_user/AppDb.dart';
import 'package:mobile/utilities/StringConstants.dart';
import 'package:moor_flutter/moor_flutter.dart';

import '../local_database_guest_user/AppDb.dart';

class DBRepositories {
  AppDb appDb = AppDb();
  Future<int> fetchLastPlayListIt() async {
    return await appDb.selectTopId().getSingle();
  }

  Future<void> insertPlayList(
      {List<ChannelsTable> channels, List<CategoriesTable> categories}) async {
    await appDb.batch((b) {
      b.insertAll(appDb.dBCategories, categories,
          mode: InsertMode.insertOrReplace);
      b.insertAll(appDb.dBChannels, channels, mode: InsertMode.insertOrReplace);
    });
  }

  Future<int> insertPlaylist(PlaylistTable playlistTable) async =>
      await appDb.into(appDb.dBPlayList).insert(playlistTable);

  Future<bool> isPlayListAvailableForGuestUser() async {
    var lengthOfPlayList = await appDb.fetchAllPlayList().get();
    return lengthOfPlayList.isNotEmpty;
  }

  Future<List<CategoriesTable>> getALlCategories() async =>
      await appDb.fetchAllCategories().get();

  Future<List<ChannelsTable>> getALlChannelsById(String id) async {
    if (id == StringConstants.FAVOURITES) {
      return await getAllFavouritesChannel();
    }
    return await appDb.fetchAllChannelById(id).get();
  }

  Future<List<ChannelsTable>> getALlChannels() async =>
      await appDb.fetchAllChannel().get();

  Future<List<ChannelsTable>> getAllFavouritesChannel() async =>
      await appDb.fetchAllFavouritesChannels().get();

  Future<List<PlaylistTable>> getALlPlayLists() async =>
      await appDb.fetchAllPlayList().get();

  Future<List<FavouritesTable>> getAllFavourites() async =>
      await appDb.fetchAllFavourites().get();

  Future<void> addFavourite(FavouritesTable table) async => await appDb
      .into(appDb.dbFavourites)
      .insert(table, mode: InsertMode.insertOrReplace);

  Future<void> removeFavourite(String channelName) async =>
      await appDb.deleteFavouritesByID(channelName);

  Future<FavouritesTable> getIsFavourite(String id) async =>
      await appDb.isFavourite(id).getSingleOrNull();

  Future<void> deletePlayLists(int id) async =>
      await appDb.transaction(() async {
        await appDb.deletePlayListByID(id);
        await appDb.deleteAllCategoriesByID(id);
        await appDb.deleteAllChannelsByID(id);
      });

  Selectable<ChannelsTable> fetchAllChannelBySearchConstraints(String var1) =>
      appDb.customSelect(
          "SELECT * FROM ChannelsTable WHERE name LIKE '%$var1%'",
          readsFrom: {appDb.dBChannels}).map(appDb.dBChannels.mapFromRow);

  Future<void> updatePlayList(String name, int id) async =>
      await appDb.updatePlayList(name, id);
}
