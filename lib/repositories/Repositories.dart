import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:mobile/local_database_guest_user/AppDb.dart';
import 'package:mobile/local_database_guest_user/injection_container.dart';
import 'package:mobile/models/activity/activity_response.dart';
import 'package:mobile/models/categories/categories.dart';
import 'package:mobile/models/categories/category_response.dart';
import 'package:mobile/models/channels/channel_response.dart';
import 'package:mobile/models/channels/channels.dart';
import 'package:mobile/models/favourites/check_favourite_response.dart';
import 'package:mobile/models/favourites/status_response.dart';
import 'package:mobile/models/login/login_response.dart';
import 'package:mobile/models/playlist/playlist_response.dart';
import 'package:mobile/models/playlist/update_response.dart';
import 'package:mobile/models/register/register_response.dart';
import 'package:mobile/utilities/StringConstants.dart';
import 'package:mobile/utilities/constants.dart';
import 'package:mobile/utilities/parser/m3u.dart';
import 'package:path_provider/path_provider.dart';

import '../local_database_guest_user/AppDb.dart';
import 'DBRepositories.dart';

class Repositories {
  final DBRepositories _localRepositories = sl();
  final String apiKey =
      "TUxrbTF1NWRNWGoxdnQ4V1htWHFWcTRYTC93d1puQVhYS1VYcmhxUnBaTEk0UDRKZnpsTWh5QzQ0dElwTWxUZE5td1hBd0ZzQ3JqeVB3RVRDVjUrVjNacE1JYTlKRTJDSGo3NHNSSTUxaFE9";

  static String baseUrl = "https://www.sageiptv.co";

  final Dio _dio = Dio();

  //urls
  var loginUrl = "$baseUrl/checkLogin";
  var registerUrl = "$baseUrl/apiRegisterUser";
  var categoriesUrl = "$baseUrl/categories";
  var channelsUrl = "$baseUrl/channels";
  var searchUrl = "$baseUrl/search";
  var activityUrl = "$baseUrl/isActivated";

  //playlists
  var playListViewUrl = "$baseUrl/apilists/view";
  var playListAddUrl = "$baseUrl/apilists/add";
  var playListEditUrl = "$baseUrl/apilists/edit";
  var playListDeleteUrl = "$baseUrl/apilists/delete";
  var playListRefreshUrl = "$baseUrl/apilists/refresh";

  var isFavouriteUrl = "$baseUrl/checkIfChannelIsInFavorites";
  var addFavouriteUrl = "$baseUrl/addToFavorites";
  var removeFavouriteUrl = "$baseUrl/removeFromFavorites";

  // app purchase status
  var _iapNotificationUrl = "$baseUrl/iapNotification";

  Future<LoginResponse> login(email, password) async {
    try {
      Response response = await _dio.get("$loginUrl/$email/$password/$apiKey");
      return LoginResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return LoginResponse.withError("$error");
    }
  }

  Future<String> saveLocalPlayList(String url) async {
    var tempDir;
    if (Platform.isAndroid) {
      tempDir = await getExternalStorageDirectory();
    } else if (Platform.isIOS) {
      tempDir = await getApplicationDocumentsDirectory();
    }

    var fileName = '/PlayList' + DateTime.now().toString() + '.m3u';

    var fullPath = tempDir.path + fileName;
    try {
      Response response = await _dio.get(
        url,
        options: Options(
          responseType: ResponseType.bytes,
          followRedirects: true,
          validateStatus: (status) {
            return status < 500;
          },
        ),
      );

      File file = File(fullPath);
      var raf = file.openSync(mode: FileMode.write);
      raf.writeFromSync(response.data);
      raf.closeSync();
      return fullPath;
    } catch (e) {
      return "";
    }
  }

  Future<void> updatePlayList({String playListName, int id}) async {
    await _localRepositories.updatePlayList(playListName, id);
  }

  Future<void> deletePlayList(int id) {
    return _localRepositories.deletePlayLists(id);
  }

  Future<RegisterResponse> register(email, password, platform) async {
    try {
      Response response =
          await _dio.get("$registerUrl/$email/$password/$apiKey/$platform");
      return RegisterResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return RegisterResponse.withError("$error");
    }
  }

  Future<CategoryResponse> getCategories(String token) async {
    try {
      if (token == null || token.isEmpty) {
        List<CategoriesTable> dbList =
            await _localRepositories.getALlCategories();
        List<CategoryListModel> list = dbList
            .map((e) =>
                CategoryListModel(id: e.id, name: e.name, counter: e.counter))
            .toList();
        List<FavouritesTable> dbFavourites =
            await _localRepositories.getAllFavourites();

        int catLength = 0;
        if (list != null) {
          catLength = list.length;

          dbFavourites.forEach((element) {
            if (list.length == catLength) {
              CategoryListModel listModel = CategoryListModel(
                  id: StringConstants.FAVOURITES,
                  counter: dbFavourites.length,
                  name: StringConstants.FAVOURITES_TITLE);
              list.insert(0, listModel);
            }
          });
        }

        Categories categories =
            Categories(list: list, poster: "", displayMode: "");
        return CategoryResponse(categories, null);
      }
      Response response = await _dio.get("$categoriesUrl/$token/$apiKey");
      return CategoryResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return CategoryResponse.withError("$error");
    }
  }

  Future<ChannelResponse> getChannels(String token, String catId) async {
    try {
      if (token == null || token.isEmpty) {
        List<ChannelsTable> dbList =
            await _localRepositories.getALlChannelsById(catId);

        List<ChannelModel> list = dbList
            .map((e) => ChannelModel(
                id: e.channelId,
                title: e.name,
                url: e.url,
                logo: e.logo,
                tvgId: ""))
            .toList();

        Channels categories = Channels(items: list);
        return ChannelResponse(categories, null);
      }
      Response response = await _dio.get("$channelsUrl/$token/$apiKey/$catId");
      return ChannelResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return ChannelResponse.withError("$error");
    }
  }

  Future<ChannelResponse> getChannelsBySearch(token, String keyword) async {
    if (token == null || token.isEmpty) {
      List<ChannelModel> list = [];
      if (keyword.isNotEmpty) {
        List<ChannelsTable> dbList = await _localRepositories
            .fetchAllChannelBySearchConstraints(keyword)
            .get();
        list = dbList
            .map((e) => ChannelModel(
                id: e.channelId,
                title: e.name,
                url: e.url,
                logo: e.logo,
                tvgId: ""))
            .toList();
        list.removeWhere((element) => !element.title
            .toLowerCase()
            .contains(keyword.toString().toLowerCase()));
      }
      Channels categories = Channels(items: list);
      return ChannelResponse(categories, null);
    }

    var url = Uri.encodeComponent(keyword);
    try {
      Response response = await _dio.get("$searchUrl/$token/$apiKey/$url");
      return ChannelResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return ChannelResponse.withError("$error");
    }
  }

  //subcription check
  Future<ActivityResponse> getActivity(token) async {
    try {
      Response response = await _dio.get("$activityUrl/$token/$apiKey");
      return ActivityResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return ActivityResponse.withError("$error");
    }
  }

  //playlists
  Future<PlayListResponse> getPlaylist(String token) async {
    try {
      if (token == null || token.isEmpty) {
        List<PlaylistTable> dbList = await _localRepositories.getALlPlayLists();
        List<PlayListModel> list = dbList
            .map((e) => PlayListModel(
                listId: e.id.toString(), listName: e.name, listUrl: e.url))
            .toList();
        PlayList categories = PlayList(lists: list);
        return PlayListResponse(categories, null);
      }
      Response response = await _dio.get("$playListViewUrl/$token/$apiKey");
      return PlayListResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return PlayListResponse.withError("$error");
    }
  }

  Future<UpdateResponse> getPlaylistAdd(token, name, url) async {
    var nameEn = Uri.encodeComponent(name);
    try {
      Response response = await _dio.get(
        "$playListAddUrl/$token/$apiKey/$nameEn",
        queryParameters: {
          'url': url,
        },
      );
      return UpdateResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return UpdateResponse.withError("$error");
    }
  }

  Future<UpdateResponse> getPlaylistEdit(token, id, name, url) async {
    var nameEn = Uri.encodeComponent(name);
    try {
      Response response = await _dio.get(
        "$playListEditUrl/$token/$apiKey/$id/$nameEn",
        queryParameters: {
          'url': url,
        },
      );
      return UpdateResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return UpdateResponse.withError("$error");
    }
  }

  Future<UpdateResponse> getPlaylistDelete(token, String id) async {
    try {
      if (token == null) {
        await _localRepositories.deletePlayLists(int.parse(id));
        Update update = Update(status: 'success');
        return UpdateResponse(update, "");
      }
      Response response =
          await _dio.get("$playListDeleteUrl/$token/$apiKey/$id");
      return UpdateResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return UpdateResponse.withError("$error");
    }
  }

  Future<UpdateResponse> getPlaylistRefresh(token, id) async {
    BaseOptions options = new BaseOptions(
      connectTimeout: 300000,
      receiveTimeout: 300000,
    );
    Dio _dio = Dio(options);
    try {
      Response response =
          await _dio.get("$playListRefreshUrl/$token/$apiKey/$id");
      return UpdateResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return UpdateResponse.withError("$error");
    }
  }

  //favourites
  Future<CheckFavouriteResponse> checkFavourite(token, title) async {
    if (token == null) {
      var favouritesModel = await _localRepositories.getIsFavourite(title);
      CheckFavourite model = CheckFavourite(isFavorite: false);
      if (favouritesModel != null) {
        model.isFavorite = true;
      }
      return CheckFavouriteResponse(model, null);
    } else {
      var titleEn = Uri.encodeComponent(title);
      try {
        Response response =
            await _dio.get("$isFavouriteUrl/$token/$apiKey/$titleEn");
        return CheckFavouriteResponse.fromJson(response.data);
      } catch (error, stacktrace) {
        return CheckFavouriteResponse.withError("$error");
      }
    }
  }

  Future<StatusResponse> addFavourite(token, title) async {
    if (token == null) {
      FavouritesTable favouritesTable = FavouritesTable(channelName: title);

      await _localRepositories.addFavourite(favouritesTable);

      Status status = Status(status: Constants.SUCCESS);
      return StatusResponse(status, null);
    } else {
      var titleEn = Uri.encodeComponent(title);
      try {
        Response response =
            await _dio.get("$addFavouriteUrl/$token/$apiKey/$titleEn");
        return StatusResponse.fromJson(response.data);
      } catch (error, stacktrace) {
        return StatusResponse.withError("$error");
      }
    }
  }

  Future<StatusResponse> removeFavourite(token, title) async {
    if (token == null) {
      await _localRepositories.removeFavourite(title);
      Status status = Status(status: Constants.SUCCESS);
      return StatusResponse(status, null);
    } else {
      var titleEn = Uri.encodeComponent(title);
      try {
        Response response =
            await _dio.get("$removeFavouriteUrl/$token/$apiKey/$titleEn");
        return StatusResponse.fromJson(response.data);
      } catch (error, stacktrace) {
        return StatusResponse.withError("$error");
      }
    }
  }

  // app purchase Notification
  Future<String> iapNotification(token, res) async {
    try {
      _dio.options.headers["apiKey"] = apiKey;
      _dio.options.headers["token"] = token;
      res.forEach((k, v) => _dio.options.headers["${k}"] = "${v}");
      Response response = await _dio.post("$_iapNotificationUrl");
      return "success";
    } catch (error, stacktrace) {
      return "error";
    }
  }

  Future<void> insertPlayListDataIntoDBCompute(
      ComputeClassForRefreshList classForRefreshList) async {
    PlaylistTable playlistTable = PlaylistTable(
        id: null,
        name: classForRefreshList.playListName,
        url: classForRefreshList.playListUrl);
    int playListId = await _localRepositories.insertPlaylist(playlistTable);

    classForRefreshList.playlistTable = PlaylistTable(
        id: playListId,
        name: classForRefreshList.playListName,
        url: classForRefreshList.playListUrl);
    var categories =
        await compute(insertPlayListDataIntoDB, classForRefreshList);
    await _localRepositories.insertPlayList(
        channels: categories.channels, categories: categories.categories);
  }
}

Future<String> readAsStringFromFilePath(String filePath) =>
    File(filePath).readAsString();

class ComputeClassForRefreshList {
  final String filePath;
  final String playListName;
  final String playListUrl;
  PlaylistTable playlistTable;

  ComputeClassForRefreshList(
      {this.filePath, this.playListName, this.playListUrl, this.playlistTable});
}

Future<DBListData> insertPlayListDataIntoDB(
    ComputeClassForRefreshList classForRefreshList) async {
  var fileContent = await File(classForRefreshList.filePath).readAsString();
  var listOfTracks =
      await parseFile(fileContent, classForRefreshList.playlistTable);
  return listOfTracks;
}

class DBListData {
  final List<ChannelsTable> channels;

  final List<CategoriesTable> categories;

  DBListData({this.channels, this.categories});
}
