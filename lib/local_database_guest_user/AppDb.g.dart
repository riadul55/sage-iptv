// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppDb.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class CategoriesTable extends DataClass implements Insertable<CategoriesTable> {
  final String id;
  final String name;
  final int playListId;
  final int counter;
  CategoriesTable(
      {@required this.id,
      @required this.name,
      @required this.playListId,
      @required this.counter});
  factory CategoriesTable.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final intType = db.typeSystem.forDartType<int>();
    return CategoriesTable(
      id: stringType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      playListId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}playListId']),
      counter:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}counter']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<String>(id);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || playListId != null) {
      map['playListId'] = Variable<int>(playListId);
    }
    if (!nullToAbsent || counter != null) {
      map['counter'] = Variable<int>(counter);
    }
    return map;
  }

  DBCategoriesCompanion toCompanion(bool nullToAbsent) {
    return DBCategoriesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      playListId: playListId == null && nullToAbsent
          ? const Value.absent()
          : Value(playListId),
      counter: counter == null && nullToAbsent
          ? const Value.absent()
          : Value(counter),
    );
  }

  factory CategoriesTable.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return CategoriesTable(
      id: serializer.fromJson<String>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      playListId: serializer.fromJson<int>(json['playListId']),
      counter: serializer.fromJson<int>(json['counter']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'name': serializer.toJson<String>(name),
      'playListId': serializer.toJson<int>(playListId),
      'counter': serializer.toJson<int>(counter),
    };
  }

  CategoriesTable copyWith(
          {String id, String name, int playListId, int counter}) =>
      CategoriesTable(
        id: id ?? this.id,
        name: name ?? this.name,
        playListId: playListId ?? this.playListId,
        counter: counter ?? this.counter,
      );
  @override
  String toString() {
    return (StringBuffer('CategoriesTable(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('playListId: $playListId, ')
          ..write('counter: $counter')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(name.hashCode, $mrjc(playListId.hashCode, counter.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is CategoriesTable &&
          other.id == this.id &&
          other.name == this.name &&
          other.playListId == this.playListId &&
          other.counter == this.counter);
}

class DBCategoriesCompanion extends UpdateCompanion<CategoriesTable> {
  final Value<String> id;
  final Value<String> name;
  final Value<int> playListId;
  final Value<int> counter;
  const DBCategoriesCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.playListId = const Value.absent(),
    this.counter = const Value.absent(),
  });
  DBCategoriesCompanion.insert({
    @required String id,
    @required String name,
    @required int playListId,
    @required int counter,
  })  : id = Value(id),
        name = Value(name),
        playListId = Value(playListId),
        counter = Value(counter);
  static Insertable<CategoriesTable> custom({
    Expression<String> id,
    Expression<String> name,
    Expression<int> playListId,
    Expression<int> counter,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (playListId != null) 'playListId': playListId,
      if (counter != null) 'counter': counter,
    });
  }

  DBCategoriesCompanion copyWith(
      {Value<String> id,
      Value<String> name,
      Value<int> playListId,
      Value<int> counter}) {
    return DBCategoriesCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      playListId: playListId ?? this.playListId,
      counter: counter ?? this.counter,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (playListId.present) {
      map['playListId'] = Variable<int>(playListId.value);
    }
    if (counter.present) {
      map['counter'] = Variable<int>(counter.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DBCategoriesCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('playListId: $playListId, ')
          ..write('counter: $counter')
          ..write(')'))
        .toString();
  }
}

class $DBCategoriesTable extends DBCategories
    with TableInfo<$DBCategoriesTable, CategoriesTable> {
  final GeneratedDatabase _db;
  final String _alias;
  $DBCategoriesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedTextColumn _id;
  @override
  GeneratedTextColumn get id => _id ??= _constructId();
  GeneratedTextColumn _constructId() {
    return GeneratedTextColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _playListIdMeta = const VerificationMeta('playListId');
  GeneratedIntColumn _playListId;
  @override
  GeneratedIntColumn get playListId => _playListId ??= _constructPlayListId();
  GeneratedIntColumn _constructPlayListId() {
    return GeneratedIntColumn(
      'playListId',
      $tableName,
      false,
    );
  }

  final VerificationMeta _counterMeta = const VerificationMeta('counter');
  GeneratedIntColumn _counter;
  @override
  GeneratedIntColumn get counter => _counter ??= _constructCounter();
  GeneratedIntColumn _constructCounter() {
    return GeneratedIntColumn(
      'counter',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, name, playListId, counter];
  @override
  $DBCategoriesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'CategoriesTable';
  @override
  final String actualTableName = 'CategoriesTable';
  @override
  VerificationContext validateIntegrity(Insertable<CategoriesTable> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('playListId')) {
      context.handle(
          _playListIdMeta,
          playListId.isAcceptableOrUnknown(
              data['playListId'], _playListIdMeta));
    } else if (isInserting) {
      context.missing(_playListIdMeta);
    }
    if (data.containsKey('counter')) {
      context.handle(_counterMeta,
          counter.isAcceptableOrUnknown(data['counter'], _counterMeta));
    } else if (isInserting) {
      context.missing(_counterMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  CategoriesTable map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return CategoriesTable.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $DBCategoriesTable createAlias(String alias) {
    return $DBCategoriesTable(_db, alias);
  }
}

class ChannelsTable extends DataClass implements Insertable<ChannelsTable> {
  final int id;
  final String channelId;
  final String name;
  final String logo;
  final String url;
  final String categoryId;
  final int playListId;
  ChannelsTable(
      {@required this.id,
      @required this.channelId,
      @required this.name,
      this.logo,
      @required this.url,
      @required this.categoryId,
      @required this.playListId});
  factory ChannelsTable.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return ChannelsTable(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      channelId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}channelId']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      logo: stringType.mapFromDatabaseResponse(data['${effectivePrefix}logo']),
      url: stringType.mapFromDatabaseResponse(data['${effectivePrefix}url']),
      categoryId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}categoryId']),
      playListId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}playListId']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || channelId != null) {
      map['channelId'] = Variable<String>(channelId);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || logo != null) {
      map['logo'] = Variable<String>(logo);
    }
    if (!nullToAbsent || url != null) {
      map['url'] = Variable<String>(url);
    }
    if (!nullToAbsent || categoryId != null) {
      map['categoryId'] = Variable<String>(categoryId);
    }
    if (!nullToAbsent || playListId != null) {
      map['playListId'] = Variable<int>(playListId);
    }
    return map;
  }

  DBChannelsCompanion toCompanion(bool nullToAbsent) {
    return DBChannelsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      channelId: channelId == null && nullToAbsent
          ? const Value.absent()
          : Value(channelId),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      logo: logo == null && nullToAbsent ? const Value.absent() : Value(logo),
      url: url == null && nullToAbsent ? const Value.absent() : Value(url),
      categoryId: categoryId == null && nullToAbsent
          ? const Value.absent()
          : Value(categoryId),
      playListId: playListId == null && nullToAbsent
          ? const Value.absent()
          : Value(playListId),
    );
  }

  factory ChannelsTable.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return ChannelsTable(
      id: serializer.fromJson<int>(json['id']),
      channelId: serializer.fromJson<String>(json['channelId']),
      name: serializer.fromJson<String>(json['name']),
      logo: serializer.fromJson<String>(json['logo']),
      url: serializer.fromJson<String>(json['url']),
      categoryId: serializer.fromJson<String>(json['categoryId']),
      playListId: serializer.fromJson<int>(json['playListId']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'channelId': serializer.toJson<String>(channelId),
      'name': serializer.toJson<String>(name),
      'logo': serializer.toJson<String>(logo),
      'url': serializer.toJson<String>(url),
      'categoryId': serializer.toJson<String>(categoryId),
      'playListId': serializer.toJson<int>(playListId),
    };
  }

  ChannelsTable copyWith(
          {int id,
          String channelId,
          String name,
          String logo,
          String url,
          String categoryId,
          int playListId}) =>
      ChannelsTable(
        id: id ?? this.id,
        channelId: channelId ?? this.channelId,
        name: name ?? this.name,
        logo: logo ?? this.logo,
        url: url ?? this.url,
        categoryId: categoryId ?? this.categoryId,
        playListId: playListId ?? this.playListId,
      );
  @override
  String toString() {
    return (StringBuffer('ChannelsTable(')
          ..write('id: $id, ')
          ..write('channelId: $channelId, ')
          ..write('name: $name, ')
          ..write('logo: $logo, ')
          ..write('url: $url, ')
          ..write('categoryId: $categoryId, ')
          ..write('playListId: $playListId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          channelId.hashCode,
          $mrjc(
              name.hashCode,
              $mrjc(
                  logo.hashCode,
                  $mrjc(url.hashCode,
                      $mrjc(categoryId.hashCode, playListId.hashCode)))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is ChannelsTable &&
          other.id == this.id &&
          other.channelId == this.channelId &&
          other.name == this.name &&
          other.logo == this.logo &&
          other.url == this.url &&
          other.categoryId == this.categoryId &&
          other.playListId == this.playListId);
}

class DBChannelsCompanion extends UpdateCompanion<ChannelsTable> {
  final Value<int> id;
  final Value<String> channelId;
  final Value<String> name;
  final Value<String> logo;
  final Value<String> url;
  final Value<String> categoryId;
  final Value<int> playListId;
  const DBChannelsCompanion({
    this.id = const Value.absent(),
    this.channelId = const Value.absent(),
    this.name = const Value.absent(),
    this.logo = const Value.absent(),
    this.url = const Value.absent(),
    this.categoryId = const Value.absent(),
    this.playListId = const Value.absent(),
  });
  DBChannelsCompanion.insert({
    this.id = const Value.absent(),
    @required String channelId,
    @required String name,
    this.logo = const Value.absent(),
    @required String url,
    @required String categoryId,
    @required int playListId,
  })  : channelId = Value(channelId),
        name = Value(name),
        url = Value(url),
        categoryId = Value(categoryId),
        playListId = Value(playListId);
  static Insertable<ChannelsTable> custom({
    Expression<int> id,
    Expression<String> channelId,
    Expression<String> name,
    Expression<String> logo,
    Expression<String> url,
    Expression<String> categoryId,
    Expression<int> playListId,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (channelId != null) 'channelId': channelId,
      if (name != null) 'name': name,
      if (logo != null) 'logo': logo,
      if (url != null) 'url': url,
      if (categoryId != null) 'categoryId': categoryId,
      if (playListId != null) 'playListId': playListId,
    });
  }

  DBChannelsCompanion copyWith(
      {Value<int> id,
      Value<String> channelId,
      Value<String> name,
      Value<String> logo,
      Value<String> url,
      Value<String> categoryId,
      Value<int> playListId}) {
    return DBChannelsCompanion(
      id: id ?? this.id,
      channelId: channelId ?? this.channelId,
      name: name ?? this.name,
      logo: logo ?? this.logo,
      url: url ?? this.url,
      categoryId: categoryId ?? this.categoryId,
      playListId: playListId ?? this.playListId,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (channelId.present) {
      map['channelId'] = Variable<String>(channelId.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (logo.present) {
      map['logo'] = Variable<String>(logo.value);
    }
    if (url.present) {
      map['url'] = Variable<String>(url.value);
    }
    if (categoryId.present) {
      map['categoryId'] = Variable<String>(categoryId.value);
    }
    if (playListId.present) {
      map['playListId'] = Variable<int>(playListId.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DBChannelsCompanion(')
          ..write('id: $id, ')
          ..write('channelId: $channelId, ')
          ..write('name: $name, ')
          ..write('logo: $logo, ')
          ..write('url: $url, ')
          ..write('categoryId: $categoryId, ')
          ..write('playListId: $playListId')
          ..write(')'))
        .toString();
  }
}

class $DBChannelsTable extends DBChannels
    with TableInfo<$DBChannelsTable, ChannelsTable> {
  final GeneratedDatabase _db;
  final String _alias;
  $DBChannelsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _channelIdMeta = const VerificationMeta('channelId');
  GeneratedTextColumn _channelId;
  @override
  GeneratedTextColumn get channelId => _channelId ??= _constructChannelId();
  GeneratedTextColumn _constructChannelId() {
    return GeneratedTextColumn(
      'channelId',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _logoMeta = const VerificationMeta('logo');
  GeneratedTextColumn _logo;
  @override
  GeneratedTextColumn get logo => _logo ??= _constructLogo();
  GeneratedTextColumn _constructLogo() {
    return GeneratedTextColumn(
      'logo',
      $tableName,
      true,
    );
  }

  final VerificationMeta _urlMeta = const VerificationMeta('url');
  GeneratedTextColumn _url;
  @override
  GeneratedTextColumn get url => _url ??= _constructUrl();
  GeneratedTextColumn _constructUrl() {
    return GeneratedTextColumn(
      'url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _categoryIdMeta = const VerificationMeta('categoryId');
  GeneratedTextColumn _categoryId;
  @override
  GeneratedTextColumn get categoryId => _categoryId ??= _constructCategoryId();
  GeneratedTextColumn _constructCategoryId() {
    return GeneratedTextColumn(
      'categoryId',
      $tableName,
      false,
    );
  }

  final VerificationMeta _playListIdMeta = const VerificationMeta('playListId');
  GeneratedIntColumn _playListId;
  @override
  GeneratedIntColumn get playListId => _playListId ??= _constructPlayListId();
  GeneratedIntColumn _constructPlayListId() {
    return GeneratedIntColumn(
      'playListId',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, channelId, name, logo, url, categoryId, playListId];
  @override
  $DBChannelsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'ChannelsTable';
  @override
  final String actualTableName = 'ChannelsTable';
  @override
  VerificationContext validateIntegrity(Insertable<ChannelsTable> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('channelId')) {
      context.handle(_channelIdMeta,
          channelId.isAcceptableOrUnknown(data['channelId'], _channelIdMeta));
    } else if (isInserting) {
      context.missing(_channelIdMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('logo')) {
      context.handle(
          _logoMeta, logo.isAcceptableOrUnknown(data['logo'], _logoMeta));
    }
    if (data.containsKey('url')) {
      context.handle(
          _urlMeta, url.isAcceptableOrUnknown(data['url'], _urlMeta));
    } else if (isInserting) {
      context.missing(_urlMeta);
    }
    if (data.containsKey('categoryId')) {
      context.handle(
          _categoryIdMeta,
          categoryId.isAcceptableOrUnknown(
              data['categoryId'], _categoryIdMeta));
    } else if (isInserting) {
      context.missing(_categoryIdMeta);
    }
    if (data.containsKey('playListId')) {
      context.handle(
          _playListIdMeta,
          playListId.isAcceptableOrUnknown(
              data['playListId'], _playListIdMeta));
    } else if (isInserting) {
      context.missing(_playListIdMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  ChannelsTable map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ChannelsTable.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $DBChannelsTable createAlias(String alias) {
    return $DBChannelsTable(_db, alias);
  }
}

class FavouritesTable extends DataClass implements Insertable<FavouritesTable> {
  final String channelName;
  FavouritesTable({@required this.channelName});
  factory FavouritesTable.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return FavouritesTable(
      channelName: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}channelName']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || channelName != null) {
      map['channelName'] = Variable<String>(channelName);
    }
    return map;
  }

  DbFavouritesCompanion toCompanion(bool nullToAbsent) {
    return DbFavouritesCompanion(
      channelName: channelName == null && nullToAbsent
          ? const Value.absent()
          : Value(channelName),
    );
  }

  factory FavouritesTable.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return FavouritesTable(
      channelName: serializer.fromJson<String>(json['channelName']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'channelName': serializer.toJson<String>(channelName),
    };
  }

  FavouritesTable copyWith({String channelName}) => FavouritesTable(
        channelName: channelName ?? this.channelName,
      );
  @override
  String toString() {
    return (StringBuffer('FavouritesTable(')
          ..write('channelName: $channelName')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf(channelName.hashCode);
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is FavouritesTable && other.channelName == this.channelName);
}

class DbFavouritesCompanion extends UpdateCompanion<FavouritesTable> {
  final Value<String> channelName;
  const DbFavouritesCompanion({
    this.channelName = const Value.absent(),
  });
  DbFavouritesCompanion.insert({
    @required String channelName,
  }) : channelName = Value(channelName);
  static Insertable<FavouritesTable> custom({
    Expression<String> channelName,
  }) {
    return RawValuesInsertable({
      if (channelName != null) 'channelName': channelName,
    });
  }

  DbFavouritesCompanion copyWith({Value<String> channelName}) {
    return DbFavouritesCompanion(
      channelName: channelName ?? this.channelName,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (channelName.present) {
      map['channelName'] = Variable<String>(channelName.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DbFavouritesCompanion(')
          ..write('channelName: $channelName')
          ..write(')'))
        .toString();
  }
}

class $DbFavouritesTable extends DbFavourites
    with TableInfo<$DbFavouritesTable, FavouritesTable> {
  final GeneratedDatabase _db;
  final String _alias;
  $DbFavouritesTable(this._db, [this._alias]);
  final VerificationMeta _channelNameMeta =
      const VerificationMeta('channelName');
  GeneratedTextColumn _channelName;
  @override
  GeneratedTextColumn get channelName =>
      _channelName ??= _constructChannelName();
  GeneratedTextColumn _constructChannelName() {
    return GeneratedTextColumn(
      'channelName',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [channelName];
  @override
  $DbFavouritesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'FavouritesTable';
  @override
  final String actualTableName = 'FavouritesTable';
  @override
  VerificationContext validateIntegrity(Insertable<FavouritesTable> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('channelName')) {
      context.handle(
          _channelNameMeta,
          channelName.isAcceptableOrUnknown(
              data['channelName'], _channelNameMeta));
    } else if (isInserting) {
      context.missing(_channelNameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  FavouritesTable map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return FavouritesTable.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $DbFavouritesTable createAlias(String alias) {
    return $DbFavouritesTable(_db, alias);
  }
}

class PlaylistTable extends DataClass implements Insertable<PlaylistTable> {
  final int id;
  final String name;
  final String url;
  PlaylistTable({@required this.id, @required this.name, @required this.url});
  factory PlaylistTable.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return PlaylistTable(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      url: stringType.mapFromDatabaseResponse(data['${effectivePrefix}url']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || url != null) {
      map['url'] = Variable<String>(url);
    }
    return map;
  }

  DBPlayListCompanion toCompanion(bool nullToAbsent) {
    return DBPlayListCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      url: url == null && nullToAbsent ? const Value.absent() : Value(url),
    );
  }

  factory PlaylistTable.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return PlaylistTable(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      url: serializer.fromJson<String>(json['url']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'url': serializer.toJson<String>(url),
    };
  }

  PlaylistTable copyWith({int id, String name, String url}) => PlaylistTable(
        id: id ?? this.id,
        name: name ?? this.name,
        url: url ?? this.url,
      );
  @override
  String toString() {
    return (StringBuffer('PlaylistTable(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('url: $url')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(name.hashCode, url.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is PlaylistTable &&
          other.id == this.id &&
          other.name == this.name &&
          other.url == this.url);
}

class DBPlayListCompanion extends UpdateCompanion<PlaylistTable> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> url;
  const DBPlayListCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.url = const Value.absent(),
  });
  DBPlayListCompanion.insert({
    this.id = const Value.absent(),
    @required String name,
    @required String url,
  })  : name = Value(name),
        url = Value(url);
  static Insertable<PlaylistTable> custom({
    Expression<int> id,
    Expression<String> name,
    Expression<String> url,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (url != null) 'url': url,
    });
  }

  DBPlayListCompanion copyWith(
      {Value<int> id, Value<String> name, Value<String> url}) {
    return DBPlayListCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      url: url ?? this.url,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (url.present) {
      map['url'] = Variable<String>(url.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DBPlayListCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('url: $url')
          ..write(')'))
        .toString();
  }
}

class $DBPlayListTable extends DBPlayList
    with TableInfo<$DBPlayListTable, PlaylistTable> {
  final GeneratedDatabase _db;
  final String _alias;
  $DBPlayListTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _urlMeta = const VerificationMeta('url');
  GeneratedTextColumn _url;
  @override
  GeneratedTextColumn get url => _url ??= _constructUrl();
  GeneratedTextColumn _constructUrl() {
    return GeneratedTextColumn(
      'url',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, name, url];
  @override
  $DBPlayListTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'PlaylistTable';
  @override
  final String actualTableName = 'PlaylistTable';
  @override
  VerificationContext validateIntegrity(Insertable<PlaylistTable> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('url')) {
      context.handle(
          _urlMeta, url.isAcceptableOrUnknown(data['url'], _urlMeta));
    } else if (isInserting) {
      context.missing(_urlMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  PlaylistTable map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return PlaylistTable.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $DBPlayListTable createAlias(String alias) {
    return $DBPlayListTable(_db, alias);
  }
}

abstract class _$AppDb extends GeneratedDatabase {
  _$AppDb(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $DBCategoriesTable _dBCategories;
  $DBCategoriesTable get dBCategories =>
      _dBCategories ??= $DBCategoriesTable(this);
  $DBChannelsTable _dBChannels;
  $DBChannelsTable get dBChannels => _dBChannels ??= $DBChannelsTable(this);
  $DbFavouritesTable _dbFavourites;
  $DbFavouritesTable get dbFavourites =>
      _dbFavourites ??= $DbFavouritesTable(this);
  $DBPlayListTable _dBPlayList;
  $DBPlayListTable get dBPlayList => _dBPlayList ??= $DBPlayListTable(this);
  Selectable<FavouritesTable> isFavourite(String var1) {
    return customSelect('Select * FROM FavouritesTable WHERE channelName = ?',
        variables: [Variable<String>(var1)],
        readsFrom: {dbFavourites}).map(dbFavourites.mapFromRow);
  }

  Future<int> deleteFavouritesByID(String var1) {
    return customUpdate(
      'Delete FROM FavouritesTable WHERE channelName = ?',
      variables: [Variable<String>(var1)],
      updates: {dbFavourites},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllChannelsByID(int var1) {
    return customUpdate(
      'Delete FROM ChannelsTable WHERE playListId = ?',
      variables: [Variable<int>(var1)],
      updates: {dBChannels},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllCategoriesByID(int var1) {
    return customUpdate(
      'Delete FROM CategoriesTable WHERE playListId = ?',
      variables: [Variable<int>(var1)],
      updates: {dBCategories},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deletePlayListByID(int var1) {
    return customUpdate(
      'Delete FROM PlaylistTable WHERE id = ?',
      variables: [Variable<int>(var1)],
      updates: {dBPlayList},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllPlayList() {
    return customUpdate(
      'Delete FROM PlaylistTable',
      variables: [],
      updates: {dBPlayList},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllCategories() {
    return customUpdate(
      'Delete FROM CategoriesTable',
      variables: [],
      updates: {dBCategories},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllFavourites() {
    return customUpdate(
      'Delete FROM FavouritesTable',
      variables: [],
      updates: {dbFavourites},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllChannels() {
    return customUpdate(
      'Delete FROM ChannelsTable',
      variables: [],
      updates: {dBChannels},
      updateKind: UpdateKind.delete,
    );
  }

  Selectable<PlaylistTable> fetchAllPlayList() {
    return customSelect('Select * FROM PlaylistTable',
        variables: [], readsFrom: {dBPlayList}).map(dBPlayList.mapFromRow);
  }

  Future<int> updatePlayList(String var1, int var2) {
    return customUpdate(
      'UPDATE  PlaylistTable SET name = ? where id = ?',
      variables: [Variable<String>(var1), Variable<int>(var2)],
      updates: {dBPlayList},
      updateKind: UpdateKind.update,
    );
  }

  Selectable<CategoriesTable> fetchAllCategories() {
    return customSelect('Select * FROM CategoriesTable',
        variables: [], readsFrom: {dBCategories}).map(dBCategories.mapFromRow);
  }

  Selectable<ChannelsTable> fetchAllChannel() {
    return customSelect('Select * FROM ChannelsTable',
        variables: [], readsFrom: {dBChannels}).map(dBChannels.mapFromRow);
  }

  Selectable<FavouritesTable> fetchAllFavourites() {
    return customSelect('Select * FROM FavouritesTable',
        variables: [], readsFrom: {dbFavourites}).map(dbFavourites.mapFromRow);
  }

  Selectable<ChannelsTable> fetchAllFavouritesChannels() {
    return customSelect(
        'Select  CHNL.* from ChannelsTable CHNL WHERE CHNL.id IN ( Select  MIN(CT.id) From ChannelsTable CT INNER JOIN FavouritesTable FT ON CT.name = FT.channelName GROUP BY CT.name )',
        variables: [],
        readsFrom: {dBChannels, dbFavourites}).map(dBChannels.mapFromRow);
  }

  Selectable<ChannelsTable> fetchAllChannelById(String var1) {
    return customSelect('Select * FROM ChannelsTable WHERE categoryId = ?',
        variables: [Variable<String>(var1)],
        readsFrom: {dBChannels}).map(dBChannels.mapFromRow);
  }

  Selectable<FavouritesTable> fetchFavouritesChannel(String var1) {
    return customSelect('Select * FROM FavouritesTable WHERE channelName = ?',
        variables: [Variable<String>(var1)],
        readsFrom: {dbFavourites}).map(dbFavourites.mapFromRow);
  }

  Selectable<int> selectTopId() {
    return customSelect(
        'SELECT id FROM PlaylistTable WHERE id = (SELECT MAX(id) FROM PlaylistTable)',
        variables: [],
        readsFrom: {dBPlayList}).map((QueryRow row) => row.read<int>('id'));
  }

  Selectable<CategoriesTable> fetchAllCategoriesById(int var1) {
    return customSelect('Select * FROM CategoriesTable WHERE playListId = ?',
        variables: [Variable<int>(var1)],
        readsFrom: {dBCategories}).map(dBCategories.mapFromRow);
  }

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [dBCategories, dBChannels, dbFavourites, dBPlayList];
}
