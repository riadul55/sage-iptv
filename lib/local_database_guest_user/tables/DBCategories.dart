import 'package:moor_flutter/moor_flutter.dart';

@DataClassName("CategoriesTable")
class DBCategories extends Table {
  TextColumn get id => text().named('id')();

  TextColumn get name => text()();

  IntColumn get playListId => integer().named('playListId')();

  IntColumn get counter => integer().named('counter')();

  @override
  Set<Column> get primaryKey => {id};

  @override
  String get tableName => 'CategoriesTable';
}
