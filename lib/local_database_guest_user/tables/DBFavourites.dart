import 'package:moor_flutter/moor_flutter.dart';

@DataClassName("FavouritesTable")
class DbFavourites extends Table {
  TextColumn get channelName => text().named('channelName')();

  @override
  String get tableName => 'FavouritesTable';
}
