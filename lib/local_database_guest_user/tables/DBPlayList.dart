import 'package:moor_flutter/moor_flutter.dart';

@DataClassName("PlaylistTable")
class DBPlayList extends Table {
  IntColumn get id => integer().named('id').autoIncrement()();

  TextColumn get name => text().named('name')();

  TextColumn get url => text().named('url')();

  @override
  String get tableName => 'PlaylistTable';
}
