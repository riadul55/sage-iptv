import 'package:moor_flutter/moor_flutter.dart';

@DataClassName("ChannelsTable")
class DBChannels extends Table {
  IntColumn get id => integer().named('id').autoIncrement()();

  TextColumn get channelId => text().named('channelId')();

  TextColumn get name => text()();

  TextColumn get logo => text().nullable()();

  TextColumn get url => text()();

  TextColumn get categoryId => text().named('categoryId')();

  IntColumn get playListId => integer().named('playListId')();

  @override
  String get tableName => 'ChannelsTable';
}
