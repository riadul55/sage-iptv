import 'package:mobile/local_database_guest_user/tables/DBCategories.dart';
import 'package:mobile/local_database_guest_user/tables/DBChannels.dart';
import 'package:mobile/local_database_guest_user/tables/DBFavourites.dart';
import 'package:mobile/local_database_guest_user/tables/DBPlayList.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'AppDb.g.dart';

@UseMoor(
  tables: [DBCategories, DBChannels, DbFavourites, DBPlayList],
  queries: {
    'isFavourite': 'Select * FROM FavouritesTable WHERE channelName = ?',
    'deleteFavouritesByID': 'Delete FROM FavouritesTable WHERE channelName = ?',
    'deleteAllChannelsByID': 'Delete FROM ChannelsTable WHERE playListId = ?',
    'deleteAllCategoriesByID':
        'Delete FROM CategoriesTable WHERE playListId = ?',
    'deletePlayListByID': 'Delete FROM PlaylistTable WHERE id = ?',
    'deleteAllPlayList': 'Delete FROM PlaylistTable',
    'deleteAllCategories': 'Delete FROM CategoriesTable',
    'deleteAllFavourites': 'Delete FROM FavouritesTable',
    'deleteAllChannels': 'Delete FROM ChannelsTable',
    'fetchAllPlayList': 'Select * FROM PlaylistTable',
    'updatePlayList': 'UPDATE  PlaylistTable SET name = ? where id = ?',
    'fetchAllCategories': 'Select * FROM CategoriesTable',
    'fetchAllChannel': 'Select * FROM ChannelsTable',
    'fetchAllFavourites': 'Select * FROM FavouritesTable',
    'fetchAllFavouritesChannels':
        'Select  CHNL.* from ChannelsTable CHNL WHERE CHNL.id IN ( Select  MIN(CT.id) From ChannelsTable CT INNER JOIN FavouritesTable FT ON CT.name = FT.channelName GROUP BY CT.name )',
    'fetchAllChannelById': 'Select * FROM ChannelsTable WHERE categoryId = ?',
    'fetchFavouritesChannel':
        'Select * FROM FavouritesTable WHERE channelName = ?',
    'selectTopId':
        'SELECT id FROM PlaylistTable WHERE id = (SELECT MAX(id) FROM PlaylistTable)',
    'fetchAllCategoriesById':
        'Select * FROM CategoriesTable WHERE playListId = ?'
  },
)
// _$AppDatabase is the name of the generated class
//Don't delete this command => flutter packages pub run build_runner watch
class AppDb extends _$AppDb {
  AppDb()
      : super((FlutterQueryExecutor.inDatabaseFolder(
          path: 'sageiptv-db',
        )));

  @override
  int get schemaVersion => 1;
}
