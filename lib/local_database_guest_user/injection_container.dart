import 'package:get_it/get_it.dart';
import 'package:mobile/repositories/DBRepositories.dart';

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerLazySingleton<DBRepositories>(
    () => DBRepositories(),
  );
}
