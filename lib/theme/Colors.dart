import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class Colors {
  const Colors();

  static Color primaryColor = HexColor("#662D91");
  static Color secondaryColor = HexColor("#E4E4E4");
  static Color accentColor = HexColor("#373A42");

  static Color primaryTextColor = HexColor("#373A42");
  static Color secondaryTextColor = HexColor("#373A42");
}
