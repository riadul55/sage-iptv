import 'package:flutter/material.dart';
import 'package:mobile/api_bloc/get_activity_bloc.dart';
import 'package:mobile/models/activity/activity_response.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/constants.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AboutScreen extends StatefulWidget {
  final BuildContext context;

  AboutScreen({Key key, this.context}) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  String token;
  String userEmail;
  bool showAds = true;

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: '1.0.0',
    buildNumber: 'Unknown',
  );

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    prefs.then((value) {
      token = value.getString(Constants.USER_TOKEN);
      if(token == null) {
        prefs.then((ads) {
          if (ads != null && ads.getBool('showAds') == false) {
            showAds = false;
          }
        });
      }
      userEmail = value.getString(Constants.USER_EMAIL);
      activityBloc..getActivity(token);
    }).catchError((onError) {
      Constants.showToast(context, "You need to login to perform this action.");
    });
    _initPackageInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.primaryColor,
      appBar: AppBar(
        backgroundColor: Style.Colors.primaryColor,
        centerTitle: true,
        title: Text("About SageIPTV"),
      ),
      body: StreamBuilder<ActivityResponse>(
        stream: activityBloc.subject.stream,
        builder: (context, AsyncSnapshot<ActivityResponse> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return Container();
            }
            // when result success
            return Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        left: 50,
                        right: 50,
                        bottom: 20,
                      ),
                      child: Image.asset(Constants.LOGO_NAME),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Text(
                      userEmail != null ? userEmail : "",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Version " + _packageInfo.version,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      (!snapshot.data.isGuestUser &&
                              snapshot.data.activity.isActivated) || (snapshot.data.isGuestUser && !showAds)
                          ? "Premium Access"
                          : "Free Access",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(
                      height: 300,
                    ),
                  ],
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Container();
          } else {
            return _buildLoadingWidget();
          }
        },
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }
}
