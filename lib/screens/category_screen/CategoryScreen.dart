import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:mobile/models/categories/categories.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/StringConstants.dart';
import 'package:mobile/utilities/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'CatToChannels.dart';

class CategoryScreen extends StatefulWidget {
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController _textController = TextEditingController();
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  Repositories repositories = new Repositories();

  SearchBar searchBar;
  String token;

  List<CategoryListModel> categories;
  List<CategoryListModel> filters = [];
  bool isLoading = true;
  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        backgroundColor: Style.Colors.primaryColor,
        title: new Text('Categories'),
        actions: [searchBar.getSearchAction(context)]);
  }

  _CategoryScreenState() {
    searchBar = new SearchBar(
        inBar: false,
        setState: setState,
        onSubmitted: search,
        onChanged: search,
        controller: _textController,
        buildDefaultAppBar: buildAppBar);
  }

  @override
  void initState() {
    _textController.addListener(() {
      search(_textController.text.toLowerCase());
    });
    super.initState();
    prefs.then((value) {
      token = value.getString(Constants.USER_TOKEN);
      repositories.getCategories(token).then((value) {
        setState(() {
          this.categories = value.categories.list;
          this.filters = value.categories.list;
          isLoading = false;
        });
      }).catchError((onError) {
        Constants.showToast(context, onError.toString());
        setState(() {
          isLoading = false;
        });
      });
    }).catchError((onError) {
      Constants.showToast(context, "You need to login to perform this action.");
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  void search(text) {
    if (text != null &&
        text.toString().isNotEmpty &&
        text.toString().length > 0 &&
        filters != null) {
      List<CategoryListModel> searchable = [];
      for (var i = 0; i < filters.length; i++) {
        if (filters[i]
                .name
                .toLowerCase()
                .contains(text.toString().toLowerCase()) ||
            filters[i]
                .counter
                .toString()
                .toLowerCase()
                .contains(text.toString().toLowerCase())) {
          searchable.add(filters[i]);
        }
      }
      setState(() {
        categories = searchable;
      });
    } else {
      setState(() {
        categories = filters;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: searchBar.build(context),
      body: isLoading ? _buildLoadingWidget() : _buildCategoriesWidget(),
    );
  }
  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(Style.Colors.primaryColor),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("$error"),
        ],
      ),
    );
  }

  Widget _buildCategoriesWidget() {
    // this.categories = data.categories.list;
    if (categories == null && categories.length == 0) {
      return Container(
        child: Text("No Categories found!"),
      );
    } else
      return ListView.builder(
        itemCount: categories.length,
        itemBuilder: (context, index) {
          var cat = categories[index];
          return Card(
            margin: EdgeInsets.only(left: 10, right: 10, top: 20),
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            child: ListTile(
              leading: cat.poster != null
                  ? CircleAvatar(
                      backgroundColor: Style.Colors.primaryColor,
                      backgroundImage: NetworkImage(cat.poster),
                    )
                  : Icon(
                      cat.id != StringConstants.FAVOURITES
                          ? Icons.ondemand_video_sharp
                          : Icons.star,
                      color: Colors.orange,
                    ),
              title: Text(
                cat.name + "(" + cat.counter.toString() + ")",
                style: TextStyle(
                  color: Style.Colors.primaryColor,
                  fontSize: 20,
                ),
              ),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) {
                      return CatToChannels(
                        catId: cat.id,
                        catName: cat.name + " (" + cat.counter.toString() + ")",
                      );
                    },
                  ),
                );
              },
            ),

          );
        },
      );
  }
}
