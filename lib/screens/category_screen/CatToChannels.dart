import 'package:flutter/material.dart';
import 'package:mobile/models/channels/channel_response.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/screens/home_screen/widgets/channel_grid.dart';
import 'package:mobile/screens/home_screen/widgets/channel_list.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CatToChannels extends StatefulWidget {
  final catId;
  final catName;

  CatToChannels({Key key, this.catId, this.catName}) : super(key: key);

  @override
  _CatToChannelsState createState() => _CatToChannelsState();
}

class _CatToChannelsState extends State<CatToChannels> {
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  Repositories repositories = new Repositories();
  String token;
  ChannelResponse channels;
  bool isLoading = true;

  @override
  void initState() {
    prefs.then((value) {
      token = value.getString(Constants.USER_TOKEN);
      // channelsBloc..getChannels(token, widget.catId);

      repositories.getChannels(token, widget.catId).then((value) {
        setState(() {
          this.channels = value;
          isLoading = false;
        });
      }).catchError((onError) {
        if (!mounted) {
          Constants.showToast(context, onError.toString());
          setState(() {
            isLoading = false;
          });
        }
      });
    }).catchError((onError) {
      Constants.showToast(context, "You need to login to perform this action.");
      setState(() {
        isLoading = false;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Style.Colors.primaryColor,
        title: Text(widget.catName != null ? widget.catName : "Channels"),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.list),
            onPressed: () {
              setState(() {
                prefs.then(
                    (value) => value.setBool(Constants.IS_LIST_GRID, true));
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.grid_view),
            onPressed: () {
              setState(() {
                prefs.then(
                    (value) => value.setBool(Constants.IS_LIST_GRID, false));
              });
            },
          ),
        ],
      ),
      body: Scaffold(
        body: FutureBuilder<bool>(
          future: _getIsList(),
          builder: (BuildContext context, AsyncSnapshot<bool> snap) {
            return isLoading
                ? _buildLoadingWidget()
                : _buildChannelsWidget(snap);
          },
        ),
      ),
    );
  }

  Future<bool> _getIsList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(Constants.IS_LIST_GRID);
  }

  Widget _buildChannelsWidget(snap) {
    if (snap.hasData && snap.data) {
      return ChannelList(
        channels: channels,
      );
    } else {
      return ChannelGrid(
        channels: channels,
      );
    }
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(Style.Colors.primaryColor),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }
}
