part of 'search_bloc.dart';

@immutable
abstract class SearchState {}

class SearchInitial extends SearchState {}

class LoadingState extends SearchState {}

class QueryState extends SearchState {
  final ChannelResponse data;
  QueryState(this.data);
}
