part of 'search_bloc.dart';

@immutable
abstract class SearchEvent {}

class LoadingEvent extends SearchEvent {}

class QueryEvent extends SearchEvent {
  final token;
  final query;
  QueryEvent(this.token, this.query);
}
