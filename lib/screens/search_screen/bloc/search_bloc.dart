import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobile/models/channels/channel_response.dart';
import 'package:mobile/repositories/Repositories.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchBloc() : super(SearchInitial());

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    final Repositories _repositories = Repositories();
    if (event is QueryEvent) {
      yield LoadingState();
      ChannelResponse response =
          await _repositories.getChannelsBySearch(event.token, event.query);
      yield QueryState(response);
    } else if (event is LoadingEvent) {
      yield LoadingState();
    }
  }
}
