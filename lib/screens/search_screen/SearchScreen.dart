import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobile/api_bloc/get_channels_bloc.dart';
import 'package:mobile/models/channels/channel_response.dart';
import 'package:mobile/screens/home_screen/widgets/channel_list.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  final searchFieldController = TextEditingController();
  String token = "";

  @override
  void initState() {
    super.initState();
    prefs.then((value) {
      token = value.getString(Constants.USER_TOKEN);
      channelsBloc.drainStream();
      if (token == null) {
        channelsBloc..getChannelsBySearch(token, "");
      }
    }).catchError((onError) {
      Constants.showToast(context, "You need to login to perform this action.");
    });
  }

  @override
  void dispose() {
    channelsBloc..drainStream();
    searchFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Style.Colors.primaryColor,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            );
          },
        ),
        title: Container(
          padding: EdgeInsets.only(left: 10, right: 10),
          decoration: BoxDecoration(
            color: Style.Colors.primaryColor,
            border: Border.all(
              color: Style.Colors.primaryColor,
              width: 0.0,
            ),
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: TextField(
            controller: searchFieldController,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Search here....",
              hintStyle: TextStyle(color: Style.Colors.secondaryColor),
            ),
            onChanged: (text) {
              channelsBloc.drainStream();
              channelsBloc..getChannelsBySearch(token, text);
            },
          ),
        ),
      ),
      body: StreamBuilder<ChannelResponse>(
        stream: channelsBloc.subject.stream,
        builder: (context, AsyncSnapshot<ChannelResponse> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return _buildErrorWidget();
            }
            return _buildChannelsWidget(snapshot.data);
          } else if (snapshot.hasError) {
            return _buildErrorWidget();
          } else {
            return _buildLoadingWidget();
          }
        },
      ),
    );
  }

  Widget _buildChannelsWidget(ChannelResponse data) {
    if (data.channels != null &&
        data.channels.items != null &&
        data.channels.items.length > 0) {
      return ChannelList(
        channels: data,
      );
    } else {
      return _buildErrorWidget();
    }
  }

  Widget _buildErrorWidget({error}) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            error != null ? error : "No channels found!",
            style: TextStyle(
              color: Style.Colors.primaryColor,
              fontSize: 18,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(Style.Colors.primaryColor),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }
}
