import 'dart:async';

import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:mobile/local_database_guest_user/injection_container.dart';
import 'package:mobile/repositories/DBRepositories.dart';
import 'package:mobile/screens/main_screen/MainScreen.dart';
import 'package:mobile/screens/playlist_screen/AddPlaylistScreen.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final InAppPurchaseConnection _connection = InAppPurchaseConnection.instance;
  String token;
  List<String> purchases = [];

  @override
  void initState()  {
    super.initState();
    Future<SharedPreferences> prefs = SharedPreferences.getInstance();
    prefs.then((value) async {
      if (value.getString(Constants.USER_TOKEN) == null) {
        final QueryPurchaseDetailsResponse purchaseResponse =
            await _connection.queryPastPurchases();
        for (PurchaseDetails purchase in purchaseResponse.pastPurchases) {
          purchases.add(purchase.purchaseID);
        }
        if (purchases.length > 0) {
          setState(() {
            _saveShowAdsOff();
          });
        } else {
          setState(() {
            _saveShowAdsOn();
          });
        }
      }
    });
    Timer(
      Duration(seconds: 1),
      () {
        prefs.then((value) async {
          if (value != null && value.getString(Constants.USER_TOKEN) != null) {
            gotToMainScreen();
          } else {
            await goToPlayListScreen();
          }
        }).catchError((onError) async {
          await goToPlayListScreen();
        });
      },
    );
  }

  void gotToMainScreen() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (BuildContext context) {
          return MainScreen();
        },
      ),
    );
  }

  Future<void> goToPlayListScreen() async {
    DBRepositories dbRepositories = sl();
    bool isPlayListAvailable =
        await dbRepositories.isPlayListAvailableForGuestUser();
    if (isPlayListAvailable) {
      gotToMainScreen();
    } else {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => AddPlaylistScreen(
            isfromHome: true,
          ),
        ),
      );
    }
  }

  _saveShowAdsOff() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('showAds', false);
  }
  _saveShowAdsOn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('showAds', true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.primaryColor,
      body: Center(
        child: Container(
          padding: EdgeInsets.all(50),
          child: Container(
            width: double.infinity,
            child: Image(
              image: AssetImage(Constants.LOGO_NAME),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
      ),
    );
  }
}
