import 'dart:io';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobile/api_bloc/get_activity_bloc.dart';
import 'package:mobile/api_bloc/get_categories_bloc.dart';
import 'package:mobile/api_bloc/get_channels_bloc.dart';
import 'package:mobile/models/activity/activity_response.dart';
import 'package:mobile/models/categories/category_response.dart';
import 'package:mobile/screens/about_screen/AboutScreen.dart';
import 'package:mobile/screens/auth_screen/LoginScreen.dart';
import 'package:mobile/screens/category_screen/CategoryScreen.dart';
import 'package:mobile/screens/home_screen/HomeScreen.dart';
import 'package:mobile/screens/main_screen/bloc/main_bloc.dart';
import 'package:mobile/screens/playlist_screen/PlaylistScreen.dart';
import 'package:mobile/screens/search_screen/SearchScreen.dart';
import 'package:mobile/screens/subscribe_screen/InAppPurchage.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/StringConstants.dart';
import 'package:mobile/utilities/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  AdmobBannerSize bannerSize;
  AdmobInterstitial interstitialAd;
  AdmobReward rewardAd;

  Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  String token;
  CategoryResponse _categoryResponse;
  bool showAds = true;
  bool isProduction = false; //development or production environment

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    prefs.then((value) {
      token = value.getString(Constants.USER_TOKEN);
      if(token == null){
        prefs.then((ads) {
          if(ads != null && ads.getBool('showAds') == false){
            showAds = false;
          }
        });
      }
      activityBloc..getActivity(token);
    }).catchError((onError) {
      Constants.showToast(context, "You need to login to perform this action.");
    });

    //admobs
    bannerSize = AdmobBannerSize.BANNER;

    interstitialAd = AdmobInterstitial(
      adUnitId: getInterstitialAdUnitId(),
      listener: (AdmobAdEvent event, Map<String, dynamic> args) {
        if (event == AdmobAdEvent.closed) interstitialAd.load();
        handleEvent(event, args, 'Interstitial');
      },
    );

    rewardAd = AdmobReward(
      adUnitId: getRewardBasedVideoAdUnitId(),
      listener: (AdmobAdEvent event, Map<String, dynamic> args) {
        if (event == AdmobAdEvent.closed) rewardAd.load();
        handleEvent(event, args, 'Reward');
      },
    );

    interstitialAd.load();
    rewardAd.load();
    categoriesBloc.drainStream();
    categoriesBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MainBloc(),
      child:
          BlocBuilder<MainBloc, MainState>(builder: (blocContext, blocState) {
        if (blocState is MainInitial) {
          BlocProvider.of<MainBloc>(blocContext).add(HomeEvent());
        }
        return WillPopScope(
          child: Scaffold(
            key: _scaffoldKey,
            backgroundColor: Style.Colors.primaryColor,
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(55),
              child: AppBar(
                elevation: 4,
                backgroundColor: Style.Colors.primaryColor,
                automaticallyImplyLeading: false,
                title: Wrap(
                  children: [
                    Container(
                      height: 55,
                      child: Image.asset(Constants.LOGO_NAME),
                    ),
                  ],
                ),
                actions: [
                  IconButton(
                    icon: Icon(Icons.list),
                    onPressed: () {
                      setState(() {
                        prefs.then((value) =>
                            value.setBool(Constants.IS_LIST_GRID, true));
                      });
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.grid_view),
                    onPressed: () {
                      setState(() {
                        prefs.then((value) =>
                            value.setBool(Constants.IS_LIST_GRID, false));
                      });
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () async {
                      await Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return SearchScreen();
                          },
                        ),
                      );
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.menu),
                    onPressed: () {
                      _scaffoldKey.currentState.openDrawer();
                    },
                  ),
                ],
              ),
            ),
            drawer: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  UserAccountsDrawerHeader(
                    decoration: BoxDecoration(
                      color: Style.Colors.primaryColor,
                    ),
                    accountName: Center(
                      child: Container(
                        child: Image.asset(Constants.LOGO_NAME),
                      ),
                    ),
                    accountEmail: Text(""),
                  ),
                  // Container(
                  //   height: 1,
                  //   width: double.infinity,
                  //   color: Colors.grey[350],
                  // ),
                  SizedBox(
                    height: 10,
                  ),
                  ListTile(
                    leading: Icon(Icons.home_outlined),
                    title: Text("Home"),
                    onTap: () {
                      _scaffoldKey.currentState.openEndDrawer();
                      BlocProvider.of<MainBloc>(blocContext).add(HomeEvent());
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.category),
                    title: Text("Categories"),
                    onTap: () {
                      _scaffoldKey.currentState.openEndDrawer();
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return CategoryScreen();
                          },
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.list),
                    title: Text("Manage Playlists"),
                    onTap: () {
                      _scaffoldKey.currentState.openEndDrawer();
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return PlaylistScreen();
                          },
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(
                      FontAwesomeIcons.crown,
                      color: Colors.orange,
                      size: 18,
                    ),
                    title: Text("Premium"),
                    onTap: () {
                      _scaffoldKey.currentState.openEndDrawer();
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return InAppPurchage();
                          },
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.info),
                    title: Text("Info"),
                    onTap: () {
                      _scaffoldKey.currentState.openEndDrawer();
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return AboutScreen();
                          },
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading:
                        token == null ? Icon(Icons.login) : Icon(Icons.logout),
                    title: Text(token == null
                        ? StringConstants.LABEL_LOGIN
                        : StringConstants.LABEL_LOGOUT),
                    onTap: () {
                      categoriesBloc.drainStream();
                      categoriesBloc.dispose();
                      channelsBloc.drainStream();
                      channelsBloc.dispose();
                      Future<SharedPreferences> prefs =
                          SharedPreferences.getInstance();
                      prefs.then((value) {
                        value.remove(Constants.USER_TOKEN);
                        value.remove(Constants.USER_EMAIL);
                      });
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (BuildContext context) => LoginScreen(),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            body: HomeScreen(),
            bottomNavigationBar: Builder(
              builder: (BuildContext context) {
                return StreamBuilder<ActivityResponse>(
                  stream: activityBloc.subject.stream,
                  builder: (context, AsyncSnapshot<ActivityResponse> snapshot) {
                    if (snapshot.hasData) {
                      if(!snapshot.data.activity.showAds){
                        showAds = false;
                      }
                      if (snapshot.data.error != null &&
                          snapshot.data.error.length > 0) {
                        return _buildBottomSheet();
                      }
                      if (showAds) {
                        return Container(
                          child: AdmobBanner(
                            adUnitId: getBannerAdUnitId(),
                            adSize: bannerSize,
                            listener: (AdmobAdEvent event,
                                Map<String, dynamic> args) {
                              handleEvent(event, args, 'Banner');
                            },
                            onBannerCreated:
                                (AdmobBannerController controller) {
                              // Dispose is called automatically for you when Flutter removes the banner from the widget tree.
                              // Normally you don't need to worry about disposing this yourself, it's handled.
                              // If you need direct access to dispose, this is your guy!
                              // controller.dispose();
                            },
                          ),
                        );
                      } else {
                        return _buildBottomSheet();
                      }
                    } else if (snapshot.hasError) {
                      return _buildBottomSheet();
                    } else {
                      return _buildBottomSheet();
                    }
                  },
                );
              },
            ),
          ),
          onWillPop: _onBackPressed,
        );
      }),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Are you sure?'),
              content: Text('Do you want to exit App?'),
              actions: <Widget>[
                TextButton(
                  child: Text('No'),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                TextButton(
                  child: Text('Yes'),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                )
              ],
            );
          },
        ) ??
        false;
  }

  Widget _buildBottomSheet() {
    return Container(
      height: 0.0,
    );
  }

  Widget _buildErrorWidget(error) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            error,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) async {
    switch (event) {
      case AdmobAdEvent.loaded:
        // showSnackBar('New Admob $adType Ad loaded!');
        break;
      case AdmobAdEvent.opened:
        // showSnackBar('Admob $adType Ad opened!');
        break;
      case AdmobAdEvent.closed:
        // showSnackBar('Admob $adType Ad closed!');
        break;
      case AdmobAdEvent.failedToLoad:
        showSnackBar('Admob $adType failed to load. :(');
        break;
      case AdmobAdEvent.rewarded:
        showDialog(
          context: _scaffoldKey.currentContext,
          builder: (BuildContext context) {
            return WillPopScope(
              child: AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text('Reward callback fired. Thanks Andrew!'),
                    Text('Type: ${args['type']}'),
                    Text('Amount: ${args['amount']}'),
                  ],
                ),
              ),
              onWillPop: () async {
                _scaffoldKey.currentState.hideCurrentSnackBar();
                return true;
              },
            );
          },
        );
        break;
      default:
    }
  }

  void showSnackBar(String content) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(content),
        duration: Duration(milliseconds: 1500),
      ),
    );
  }

  String getBannerAdUnitId() {
    if (isProduction) {
      if (Platform.isIOS) {
        return 'ca-app-pub-6434673162453817/5202290796';
      } else if (Platform.isAndroid) {
        return 'ca-app-pub-6434673162453817/3014562413';
      }
    } else {
      if (Platform.isIOS) {
        return 'ca-app-pub-3940256099942544/2934735716';
      } else if (Platform.isAndroid) {
        return 'ca-app-pub-3940256099942544/6300978111';
      }
    }
    return null;
  }

  String getInterstitialAdUnitId() {
    if (isProduction) {
      if (Platform.isIOS) {
        return 'ca-app-pub-6434673162453817/7982584533';
      } else if (Platform.isAndroid) {
        return 'ca-app-pub-6434673162453817/5808557775';
      }
    } else {
      if (Platform.isIOS) {
        return 'ca-app-pub-3940256099942544/4411468910';
      } else if (Platform.isAndroid) {
        return 'ca-app-pub-3940256099942544/1033173712';
      }
    }
    return null;
  }

  String getRewardBasedVideoAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-3940256099942544/1712485313';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-3940256099942544/5224354917';
    }
    return null;
  }
}
