import 'package:flutter/material.dart';
import 'package:mobile/api_bloc/get_categories_bloc.dart';
import 'package:mobile/models/categories/categories.dart';
import 'package:mobile/models/categories/category_response.dart';
import 'package:mobile/screens/home_screen/widgets/categories.dart';
import 'package:mobile/utilities/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  String token;

  @override
  void initState() {
    initData();
    super.initState();
  }

  void initData() {
    prefs.then((value) {
      token = value.getString(Constants.USER_TOKEN);
      categoriesBloc..getCategories(token);
    }).catchError((onError) {
      Constants.showToast(context, "You need to login to perform this action.");
    });
  }

  @override
  void dispose() {
    categoriesBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CategoryResponse>(
      stream: categoriesBloc.subject.stream,
      builder: (context, AsyncSnapshot<CategoryResponse> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.error != null && snapshot.data.error.length > 0) {
            return _buildErrorWidget(snapshot.data.error);
          }
          return _buildCategoriesWidget(snapshot.data);
        } else if (snapshot.hasError) {
          return _buildErrorWidget(snapshot.error);
        } else {
          return _buildLoadingWidget();
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        // child: Column(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   children: <Widget>[
        //     Text("$error"),
        //   ],
        // ),
        );
  }

  Widget _buildCategoriesWidget(CategoryResponse data) {
    List<CategoryListModel> categories = data.categories.list;
    if (categories.length == 0) {
      return Container(
        child: Text("No Categories found!"),
      );
    } else
      return CategoryList(
        token: token,
        categories: categories,
      );
  }
}
