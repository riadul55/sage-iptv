import 'package:flutter/material.dart';
import 'package:mobile/api_bloc/get_channels_bloc.dart';
import 'package:mobile/models/categories/categories.dart';
import 'package:mobile/screens/home_screen/widgets/home_body.dart';
import 'package:mobile/theme/Colors.dart' as Style;

class CategoryList extends StatefulWidget {
  final List<CategoryListModel> categories;
  final String token;

  CategoryList({Key key, @required this.categories, @required this.token})
      : super(key: key);

  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList>
    with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    initData();
    super.initState();
  }

  void initData() {
    _tabController = null;
    _tabController =
        TabController(length: widget.categories.length, vsync: this);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging) {
        channelsBloc..drainStream();
      }
    });
  }

  @override
  void didUpdateWidget(covariant CategoryList oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    initData();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: DefaultTabController(
        length: widget.categories.length,
        child: Scaffold(
          backgroundColor: Style.Colors.primaryColor,
          appBar: PreferredSize(
            child: AppBar(
              backgroundColor: Style.Colors.primaryColor,
              bottom: TabBar(
                controller: _tabController,
                indicatorColor: Colors.orange,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorWeight: 4.0,
                unselectedLabelColor: Style.Colors.secondaryColor,
                labelColor: Colors.white,
                isScrollable: true,
                tabs: widget.categories.map((CategoryListModel category) {
                  return Container(
                    padding: EdgeInsets.only(bottom: 15.0, top: 10.0),
                    child: Text(
                      category.name.toUpperCase() +
                          " (" +
                          category.counter.toString() +
                          ")",
                      style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
            preferredSize: Size.fromHeight(50.0),
          ),
          body: TabBarView(
            controller: _tabController,
            physics: AlwaysScrollableScrollPhysics(),
            children: widget.categories.map((CategoryListModel category) {
              return HomeBody(
                token: widget.token,
                catId: category.id,
              );
            }).toList(),
          ),

        ),
      ),
    );
  }
}
