import 'package:flutter/material.dart';
import 'package:mobile/models/channels/channel_response.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'channel_grid.dart';
import 'channel_list.dart';

class HomeBody extends StatefulWidget {
  final String catId;
  final String token;

  HomeBody({Key key, @required this.catId, @required this.token})
      : super(key: key);

  @override
  _HomeBodyState createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> {
  Repositories repositories = new Repositories();
  ChannelResponse channels;
  bool isLoading = true;

  @override
  void initState() {
    // channelsBloc..drainStream();
    // channelsBloc..getChannels(widget.token, widget.catId);

    initData();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant HomeBody oldWidget) {
    super.didUpdateWidget(oldWidget);
    initData();
  }

  void initData() {
    repositories.getChannels(widget.token, widget.catId).then((value) {
      setState(() {
        this.channels = value;
        isLoading = false;
      });
    }).catchError((onError) {
      Constants.showToast(context, onError.toString());
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<bool>(
        future: _getIsList(),
        builder: (BuildContext context, AsyncSnapshot<bool> snap) {
          return isLoading ? _buildLoadingWidget() : _buildChannelsWidget(snap);
        },
      ),
    );
  }

  Future<bool> _getIsList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(Constants.IS_LIST_GRID);
  }

  Widget _buildChannelsWidget(snap) {
    if (snap.hasData && snap.data) {
      return ChannelList(
        channels: channels,
      );
    } else {
      return ChannelGrid(
        channels: channels,
      );
    }
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(Style.Colors.primaryColor),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }
}
