import 'package:flutter/material.dart';
import 'package:mobile/theme/Colors.dart' as Style;

class Menus extends StatefulWidget {
  final currentState;
  Menus({Key key, this.currentState}) : super(key: key);
  @override
  _MenusState createState() => _MenusState(currentState);
}

class _MenusState extends State<Menus> {
  final currentState;
  _MenusState(this.currentState);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 10,
        left: 20,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Menus",
            style: TextStyle(
              color: Style.Colors.primaryColor,
            ),
          ),
          ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.vertical,
            padding: EdgeInsets.all(0),
            children: [

            ],
          ),
        ],
      ),
    );
  }
}
