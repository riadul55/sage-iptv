import 'package:flutter/material.dart';
import 'package:mobile/models/channels/channel_response.dart';
import 'package:mobile/screens/playlist_screen/widgets/vlc_player_widget.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/default_icon.dart';

class ChannelGrid extends StatefulWidget {
  final ChannelResponse channels;

  ChannelGrid({Key key, this.channels}) : super(key: key);

  @override
  _ChannelGridState createState() => _ChannelGridState();
}

class _ChannelGridState extends State<ChannelGrid> {
  // final ChannelResponse channels;
  // _ChannelGridState(this.channels);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.count(
        crossAxisCount: 2,
        children: List.generate(
            widget.channels.channels.items != null
                ? widget.channels.channels.items.length
                : 0, (index) {
          var data = widget.channels.channels.items[index];
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return VlcPlayerWidget(
                      title: data.title,
                      url: data.url,
                    );
                  },
                ),
              );
            },
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Card(
                elevation: 4,
                child: Column(
                  children: [
                    data.logo.isNotEmpty
                        ? Container(
                            height: 120,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2.0)),
                              shape: BoxShape.rectangle,
                              image: DecorationImage(
                                  image: NetworkImage(data.logo),
                                  fit: BoxFit.cover),
                            ),
                          )
                        : Container(
                            height: 120,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2.0)),
                              shape: BoxShape.rectangle,
                            ),
                            child: DefaultIcon(),
                          ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      child: Text(
                        data.title,
                        maxLines: 2,
                        style: TextStyle(
                          height: 1.4,
                          color: Style.Colors.primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
