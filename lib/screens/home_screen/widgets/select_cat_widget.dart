import 'package:flutter/material.dart';
import 'package:mobile/theme/Colors.dart' as Style;

class SelectCatWidget extends StatefulWidget {
  @override
  _SelectCatWidgetState createState() => _SelectCatWidgetState();
}

class _SelectCatWidgetState extends State<SelectCatWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Please Select a category",
              style: TextStyle(
                color: Style.Colors.primaryColor,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              "from menu",
              style: TextStyle(
                color: Style.Colors.primaryColor,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
