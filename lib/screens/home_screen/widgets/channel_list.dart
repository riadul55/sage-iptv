import 'package:flutter/material.dart';
import 'package:mobile/models/channels/channel_response.dart';
import 'package:mobile/screens/playlist_screen/widgets/vlc_player_widget.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/default_icon.dart';

class ChannelList extends StatefulWidget {
  final ChannelResponse channels;

  ChannelList({Key key, this.channels}) : super(key: key);

  @override
  _ChannelListState createState() => _ChannelListState();
}

class _ChannelListState extends State<ChannelList> {
  // final ChannelResponse channels;
  // _ChannelListState(this.channels);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.channels.channels.items != null
          ? widget.channels.channels.items.length
          : 0,
      itemBuilder: (context, index) {
        var data = widget.channels.channels.items[index];
        return Card(
          margin: EdgeInsets.only(left: 10, right: 10, top: 20),
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4),
          ),
          child: ListTile(
            leading: data.logo.isNotEmpty
                ? CircleAvatar(
                    backgroundImage: NetworkImage(data.logo),
                  )
                : DefaultIcon(),
            title: Text(
              data.title,
              style: TextStyle(
                color: Style.Colors.primaryColor,
                fontSize: 20,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              // Constants.showToast(context, "$index");
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return VlcPlayerWidget(
                      title: data.title,
                      url: data.url,
                    );
                  },
                ),
              );
            },
          ),
        );
      },
    );
  }
}
