import 'package:flutter/material.dart';

class SubmitButton extends StatelessWidget {
  final String text;
  final Function press;
  final Color color, textColor;
  const SubmitButton({
    Key key,
    this.text,
    this.press,
    this.color,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.8,
      child: TextButton(
        style: TextButton.styleFrom(
          primary: Colors.white,
          backgroundColor: Colors.orange,
          onSurface: Colors.grey,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 40)
        ),
        onPressed: press,
        child: Text(
          text,
          style: TextStyle(color: textColor),
        ),
      ),
    );
  }
}
