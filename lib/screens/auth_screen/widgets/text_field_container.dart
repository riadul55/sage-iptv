import 'package:flutter/material.dart';
import 'package:mobile/theme/Colors.dart' as Style;

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: Style.Colors.primaryColor,
        border: Border(
          bottom: BorderSide(
            color: Style.Colors.secondaryColor,
            width: 1,
          ),
        ),
      ),
      child: child,
    );
  }
}
