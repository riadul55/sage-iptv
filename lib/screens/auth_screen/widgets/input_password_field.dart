import 'package:flutter/material.dart';
import 'package:mobile/theme/Colors.dart' as Style;

import 'text_field_container.dart';

class InputPasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  final String hintText;
  const InputPasswordField({
    Key key,
    this.onChanged,
    this.hintText = "Password",
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChanged,
        cursorColor: Style.Colors.secondaryColor,
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: TextStyle(color: Style.Colors.secondaryColor),
          icon: Icon(
            Icons.lock,
            color: Colors.white,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: Style.Colors.primaryColor,
          ),
          border: InputBorder.none,
        ),
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
}
