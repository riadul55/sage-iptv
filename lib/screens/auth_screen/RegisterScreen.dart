import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/constants.dart';

import 'LoginScreen.dart';
import 'widgets/already_have_an_account_check.dart';
import 'widgets/input_email_field.dart';
import 'widgets/input_password_field.dart';
import 'widgets/submit_button.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String email = "", password = "", confirmPass = "", platform = "Android";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Style.Colors.primaryColor,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.only(
                  left: 50,
                  right: 50,
                  bottom: 20,
                ),
                child: Image.asset(Constants.LOGO_NAME),
              ),
              InputEmailField(
                hintText: "E-mail",
                onChanged: (value) {
                  this.email = value;
                },
              ),
              InputPasswordField(
                onChanged: (value) {
                  this.password = value;
                },
              ),
              InputPasswordField(
                hintText: "Confirm Password",
                onChanged: (value) {
                  this.confirmPass = value;
                },
              ),
              SizedBox(
                height: 30,
              ),
              SubmitButton(
                text: "REGISTER",
                color: Style.Colors.secondaryColor,
                textColor: Style.Colors.primaryColor,
                press: () {
                  _submit();
                },
              ),
              SizedBox(height: size.height * 0.03),
              AlreadyHaveAnAccountCheck(
                login: false,
                press: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submit() async {
    if (email.isEmpty) {
      Constants.showToast(context, "Email field is required!");
    } else if (password.isEmpty) {
      Constants.showToast(context, "Password field is required!");
    } else if (confirmPass.isEmpty) {
      Constants.showToast(context, "Confirm Password field is required!");
    } else if (password != confirmPass) {
      Constants.showToast(context, "Password doesn't matched!");
    } else {
      _onLoading();
      Repositories repositories = new Repositories();
      if (Platform.isAndroid) {
        this.platform = "Android";
      } else if (Platform.isIOS) {
        this.platform = "iOS";
      }
      repositories.register(email, password, platform).then((value) async {
        if (value != null) {
          if (value.register != null
              && value.register.registration != null
              && value.register.registration == "failed") {
            Constants.showToast(context, value.register.reason);
          } else {
            // SharedPreferences prefs = await SharedPreferences.getInstance();
            // prefs.setString(Constants.USER_TOKEN, value.register.token);
            Constants.showToast(context, "Your account has been created successfully!");

            await Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return LoginScreen();
                },
              ),
            );
          }
        } else {
          Constants.showToast(context, value.error);
        }
        _onHideLoading();
      }).catchError((onError) {
        _onHideLoading();
        Constants.showToast(context, onError.toString());
      });
    }
  }

  void _onLoading() {
    showDialog(
      context: _scaffoldKey.currentState.context,
      barrierDismissible: false,
      builder: (_) => new SimpleDialog(
        title: Container(
          child: Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 10.0),
                  child: new Text("Loading"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onHideLoading() {
    Navigator.pop(_scaffoldKey.currentState.context);
  }
}
