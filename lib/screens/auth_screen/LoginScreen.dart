import 'package:flutter/material.dart';
import 'package:mobile/local_database_guest_user/injection_container.dart';
import 'package:mobile/repositories/DBRepositories.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/screens/auth_screen/RegisterScreen.dart';
import 'package:mobile/screens/main_screen/MainScreen.dart';
import 'package:mobile/screens/playlist_screen/AddPlaylistScreen.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/StringConstants.dart';
import 'package:mobile/utilities/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'widgets/already_have_an_account_check.dart';
import 'widgets/input_email_field.dart';
import 'widgets/input_password_field.dart';
import 'widgets/submit_button.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String email = "", password = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Style.Colors.primaryColor,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.only(
                  left: 50,
                  right: 50,
                  bottom: 20,
                ),
                child: Image.asset(Constants.LOGO_NAME),
              ),
              InputEmailField(
                hintText: "E-mail",
                onChanged: (value) {
                  this.email = value;
                },
              ),
              InputPasswordField(
                onChanged: (value) {
                  this.password = value;
                },
              ),
              SizedBox(
                height: 30,
              ),
              SubmitButton(
                text: "LOGIN",
                color: Style.Colors.secondaryColor,
                textColor: Style.Colors.primaryColor,
                press: () {
                  _submit();
                  // Navigator.pushReplacement(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) {
                  //       return HomeScreen();
                  //     },
                  //   ),
                  // );
                },
              ),
              SizedBox(height: size.height * 0.03),
              AlreadyHaveAnAccountCheck(
                press: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return RegisterScreen();
                      },
                    ),
                  );
                },
              ),
              SizedBox(height: size.height * 0.03),
              InkWell(
                onTap: () async {
                  DBRepositories dbRepositories = sl();
                  bool isPlayListAvailable =
                      await dbRepositories.isPlayListAvailableForGuestUser();
                  if (isPlayListAvailable) {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (BuildContext context) {
                          return MainScreen();
                        },
                      ),
                    );
                  } else {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (BuildContext context) => AddPlaylistScreen(
                          isfromHome: true,
                        ),
                      ),
                    );
                  }
                },
                child: Text(
                  StringConstants.CONTINUE_AS_GUEST,
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _submit() async {
    if (email.isEmpty) {
      Constants.showToast(context, "Email field is required!");
    } else if (password.isEmpty) {
      Constants.showToast(context, "Password field is required!");
    } else {
      _onLoading();
      Repositories repositories = new Repositories();
      repositories.login(email, password).then((value) async {
        if (value != null && value.login != null && value.login.token != null) {
          if (value.error != null && value.error.length > 0) {
            Constants.showToast(context, value.error);
          } else {
            // if success
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.setString(Constants.USER_TOKEN, value.login.token);
            prefs.setString(Constants.USER_EMAIL, email);

            await Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return MainScreen();
                },
              ),
            );
            Constants.showToast(context, "Login Success!");
          }
        } else {
          Constants.showToast(context, "Email or Passward Invalid!");
          _onHideLoading();
        }
      }).catchError((onError) {
        _onHideLoading();
        Constants.showToast(context, onError.toString());
      });
    }
  }

  void _onLoading() {
    showDialog(
      context: _scaffoldKey.currentState.context,
      barrierDismissible: false,
      builder: (_) => new SimpleDialog(
        title: Container(
          child: Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 10.0),
                  child: new Text("Loading"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onHideLoading() {
    Navigator.pop(_scaffoldKey.currentState.context);
  }
}
