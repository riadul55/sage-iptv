import 'dart:async';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mobile/local_database_guest_user/injection_container.dart';
import 'package:mobile/models/playlist/playlist_response.dart';
import 'package:mobile/repositories/DBRepositories.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/screens/auth_screen/LoginScreen.dart';
import 'package:mobile/screens/main_screen/MainScreen.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/PermissionUtil.dart';
import 'package:mobile/utilities/StringConstants.dart';
import 'package:mobile/utilities/constants.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:string_validator/string_validator.dart';

import 'package:mobile/screens/auth_screen/widgets/submit_button.dart';
import 'widgets/input_edittext_field.dart';

class AddPlaylistScreen extends StatefulWidget {
  PlayListModel model;
  bool isfromHome, isUpdate;

  AddPlaylistScreen(
      {Key key, this.isfromHome = false, this.isUpdate = false, this.model})
      : super(key: key);

  @override
  _AddPlaylistScreenState createState() => _AddPlaylistScreenState();
}

class _AddPlaylistScreenState extends State<AddPlaylistScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String token;
  String playlistName = "", playlistUrl = "";
  String portalUrl = "",
      portalPort = "",
      portalUsername = "",
      portalPassword = "";
  int _radioValue1 = 0, _radioUrl = 1, _radioFile = 0, _radioPortal = 2;

  StreamController<int> selectionController = StreamController<int>.broadcast();
  StreamController<String> pathController =
      StreamController<String>.broadcast();
  Repositories remoteRepo = Repositories();

  @override
  void initState() {
    super.initState();
    if (widget.isUpdate) {
      playlistName = widget.model.listName;
    }
  }

  @override
  Widget build(BuildContext _) => Scaffold(
        backgroundColor: Style.Colors.primaryColor,
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Style.Colors.primaryColor,
          centerTitle: true,
          title: titleWidget(),
          leading: widget.isfromHome ? null : closeButton(),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.04),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                playlistNameINputBox(),
                twentyHeightSizedBox(),
                if (widget.isUpdate) ...[
                  updateWidget(),
                ] else ...[
                  buildStreamBuilder(),
                ],
                twentyHeightSizedBox(),
                submitButton(),
                twentyHeightSizedBox(),
                if (widget.isfromHome) ...[alreadyHaveAccountWidget()]
              ],
            ),
          ),
        ),
      );

  Widget titleWidget() => Text(widget.isUpdate
      ? StringConstants.UPDATE_PLAYLIST
      : StringConstants.ADD_PLAYLIST);

  Widget closeButton() => InkWell(
        child: Icon(Icons.close),
        onTap: () {
          Navigator.pop(context);
        },
      );

  Widget submitButton() => SubmitButton(
        text: widget.isUpdate
            ? StringConstants.LABEL_UPDATE
            : StringConstants.LABEL_ADD,
        color: Style.Colors.secondaryColor,
        textColor: Style.Colors.primaryColor,
        press: () async {
          if (widget.isUpdate) {
            if (playlistName == null || playlistName.isEmpty) {
              Constants.showToast(context, StringConstants.ERROR_NAME_EMPTY);
            } else {
              remoteRepo.updatePlayList(
                playListName: playlistName,
                id: int.parse(widget.model.listId),
              );
              Navigator.of(context).pop();
            }
          } else {
            if (await PermissionUtil.getPermissionStatus(Permission.storage)) {
              addPlayList();
            } else {
              bool result =
                  await PermissionUtil.getStoragePermission(context: context);
              if (result) {
                addPlayList();
              }
            }
          }
        },
      );

  InputEdittextField playlistNameINputBox() => InputEdittextField(
        hintText: StringConstants.LABEL_PLAYLIST_NAME,
        icon: Icons.edit,
        text: this.playlistName,
        onChanged: (value) {
          this.playlistName = value;
        },
      );

  SizedBox twentyHeightSizedBox() => SizedBox(
        height: 20,
      );

  Row updateWidget() => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.05,
          ),
          Icon(
            Icons.link,
            color: Colors.white,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.05,
          ),
          Expanded(
            child: Text(
              widget.model.listUrl,
              style:
                  TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 12),
            ),
          )
        ],
      );

  void addPlayList() async {
    _downloadPlayList();
  }

  Widget alreadyHaveAccountWidget() => InkWell(
        onTap: () {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (BuildContext context) => LoginScreen(),
            ),
          );
        },
        child: Text(
          StringConstants.ALREADY_HAVING_ACCOUNT,
          style: TextStyle(
              decoration: TextDecoration.underline, color: Colors.white),
        ),
      );

  Widget buildStreamBuilder() => StreamBuilder<int>(
      initialData: _radioValue1,
      stream: selectionController.stream,
      builder: (_, snapshot) => Column(
            children: [
              Row(
                children: [
                  Radio(
                    activeColor: Colors.white,
                    value: _radioUrl,
                    groupValue: _radioValue1,
                    onChanged: (value) {
                      _radioValue1 = value;
                      selectionController.add(_radioValue1);
                    },
                  ),
                  InkWell(
                    onTap: () {
                      _radioValue1 = _radioUrl;
                      selectionController.add(_radioValue1);
                    },
                    child: Text(
                      StringConstants.FROM_URL,
                      style: TextStyle(fontSize: 16.0, color: Colors.white),
                    ),
                  ),
                  // Portal
                  Radio(
                    activeColor: Colors.white,
                    value: _radioPortal,
                    groupValue: _radioValue1,
                    onChanged: (value) {
                      _radioValue1 = value;
                      selectionController.add(_radioValue1);
                    },
                  ),
                  InkWell(
                    onTap: () {
                      _radioValue1 = _radioPortal;
                      selectionController.add(_radioValue1);
                    },
                    child: Text(
                      StringConstants.FROM_PORTAL,
                      style: TextStyle(fontSize: 16.0, color: Colors.white),
                    ),
                  ),
                  Radio(
                    activeColor: Colors.white,
                    value: _radioFile,
                    groupValue: _radioValue1,
                    onChanged: (value) {
                      _radioValue1 = value;
                      selectionController.add(_radioValue1);
                    },
                  ),
                  InkWell(
                    onTap: () {
                      _radioValue1 = _radioFile;
                      selectionController.add(_radioValue1);
                    },
                    child: Text(
                      StringConstants.FROM_FILE,
                      style: TextStyle(fontSize: 16.0, color: Colors.white),
                    ),
                  ),
                ],
              ),
              if (_radioValue1 == _radioUrl) ...[
                getUrlWidget(),
              ],
              if (_radioValue1 == _radioPortal) ...[
                getPortalWidget(),
              ],
              if (_radioValue1 == _radioFile) ...[
                InkWell(
                  child: getFileUploadWidget(),
                  onTap: () async {
                    FilePickerResult result =
                        await FilePicker.platform.pickFiles(
                      type: FileType.custom,
                      allowedExtensions: ['m3u'],
                    );
                    if (result != null) {
                      File file = File(result.files.single.path);
                      playlistUrl = file.path;
                    } else {
                      playlistUrl = "";
                    }
                    pathController.add(playlistUrl);
                  },
                )
              ],
            ],
          ));

  Widget getUrlWidget() => InputEdittextField(
        hintText: StringConstants.LABEL_PLAYLIST_URL,
        icon: Icons.link,
        text: playlistUrl,
        onChanged: (value) {
          this.playlistUrl = value;
        },
      );

  Widget getPortalWidget() => Container(
        //color: Color(0xff258DED),
        height: 400.0,
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height * 0.01),
              width: MediaQuery.of(context).size.width * 0.8,
              decoration: BoxDecoration(
                color: Style.Colors.primaryColor,
                border: Border(
                  bottom: BorderSide(
                    color: Style.Colors.secondaryColor,
                    width: 1,
                  ),
                ),
              ),
              child: Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.04,
                  ),
                  Icon(
                    Icons.link,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.04,
                  ),
                  Expanded(
                    child: TextFormField(
                      key: Key(portalUrl),
                      style: TextStyle(color: Colors.white),
                      obscureText: false,
                      enableSuggestions: false,
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        hintText: StringConstants.FROM_PORTAL_URL,
                        hintStyle: TextStyle(color: Colors.white),
                      ),
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return 'Url is required';
                        }
                      },
                      onChanged: (value) {
                        this.portalUrl = value;
                      },
                    ),
                  ),
                ],
              ),
            ),
            InputEdittextField(
              hintText: StringConstants.FROM_PORTAL_PORT,
              icon: Icons.edit,
              text: portalPort,
              onChanged: (value) {
                this.portalPort = value;
              },
            ),
            InputEdittextField(
              hintText: StringConstants.FROM_PORTAL_USERNAME,
              icon: Icons.edit,
              text: portalUsername,
              onChanged: (value) {
                this.portalUsername = value;
              },
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height * 0.01),
              width: MediaQuery.of(context).size.width * 0.8,
              decoration: BoxDecoration(
                color: Style.Colors.primaryColor,
                border: Border(
                  bottom: BorderSide(
                    color: Style.Colors.secondaryColor,
                    width: 1,
                  ),
                ),
              ),
              child: Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.04,
                  ),
                  Icon(
                    Icons.lock,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.04,
                  ),
                  Expanded(
                    child: TextFormField(
                      key: Key(portalPassword),
                      style: TextStyle(color: Colors.white),
                      obscureText: true,
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        hintText: StringConstants.FROM_PORTAL_PASSWORD,
                        hintStyle: TextStyle(color: Colors.white),
                      ),
                      validator: (String value) {
                        if (value.trim().isEmpty) {
                          return 'Password is required';
                        }
                      },
                      onChanged: (value) {
                        this.portalPassword = value;
                      }
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );

  Widget getFileUploadWidget() => StreamBuilder<String>(
      initialData: "",
      stream: pathController.stream,
      builder: (_, snapshot) => Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: MediaQuery.of(context).size.height * 0.025),
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
              color: Style.Colors.primaryColor,
              border: Border(
                bottom: BorderSide(
                  color: Style.Colors.secondaryColor,
                  width: 1,
                ),
              ),
            ),
            child: Row(
              children: [
                Icon(
                  Icons.file_upload,
                  color: Colors.white,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.04,
                ),
                Expanded(
                  child: Text(
                    snapshot.data.isEmpty
                        ? StringConstants.SELECT_FILE
                        : playlistUrl,
                    style: TextStyle(fontSize: 16.0, color: Colors.white),
                  ),
                ),
              ],
            ),
          ));

  void _downloadPlayList() async {
    if(_radioValue1 == _radioPortal) {
      if (portalUsername == null || portalUsername.isEmpty) {
        Constants.showToast(context, StringConstants.FROM_PORTAL_USERNAME+ " " + StringConstants.ERROR_FIELD_EMPTY);
        return;
      }
      if (portalPassword == null || portalPassword.isEmpty) {
        Constants.showToast(context, StringConstants.FROM_PORTAL_PASSWORD+ " " + StringConstants.ERROR_FIELD_EMPTY);
        return;
      }
      if (portalPort == null || portalPort.isEmpty) {
        Constants.showToast(context, StringConstants.FROM_PORTAL_PORT+ " " + StringConstants.ERROR_FIELD_EMPTY);
        return;
      }
      if(!isNumeric(portalPort)){
        Constants.showToast(context, StringConstants.FROM_PORTAL_PORT+ " must be numeric ");
        return;
      }
      if(!isURL(portalUrl)){
        Constants.showToast(context, StringConstants.FROM_PORTAL_URL+ " must be valid");
        return;
      }
      playlistUrl = portalUrl + ":" + portalPort + "/get.php?username=" + portalUsername + "&password=" + portalPassword + "&output=ts&type=m3u_plus";
    }
    if (playlistName == null || playlistName.isEmpty) {
      Constants.showToast(context, StringConstants.ERROR_NAME_EMPTY);
    } else if (playlistUrl == null || playlistUrl.isEmpty) {
      Constants.showToast(context, StringConstants.ERROR_URL_EMPTY);
    } else {
      String path = playlistUrl;
      EasyLoading.show(status: 'loading...');
      if (_radioValue1 == _radioUrl || _radioValue1 == _radioPortal) {
        path = await remoteRepo.saveLocalPlayList(playlistUrl);
      }
      if (path.isEmpty) {
        EasyLoading.dismiss().then(
          (_) =>
              Constants.showToast(context, StringConstants.ERROR_DOWNLOADING),
        );
      } else {
        try {
          ComputeClassForRefreshList classForRefreshList =
              ComputeClassForRefreshList(
                  filePath: path,
                  playListName: playlistName,
                  playListUrl: playlistUrl);
          await remoteRepo
              .insertPlayListDataIntoDBCompute(classForRefreshList)
              .then(
            (_) {
              EasyLoading.dismiss().then(
                (_) {
                  Constants.showToast(context, StringConstants.ADD_SUCCESS);
                  return Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (_) => MainScreen(),
                    ),
                  );
                },
              );
            },
          );
        } catch (e) {
          DBRepositories localDB = sl();
          await localDB.deletePlayLists(await localDB.fetchLastPlayListIt());
          await EasyLoading.dismiss();
          Constants.showToast(context, StringConstants.ERROR_DOWNLOADING);
        }
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    selectionController.close();
    pathController.close();
  }
}
