import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';
import 'package:mobile/api_bloc/get_categories_bloc.dart';
import 'package:mobile/api_bloc/get_channels_bloc.dart';
import 'package:mobile/api_bloc/get_check_favourite_bloc.dart';
import 'package:mobile/models/favourites/check_favourite_response.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wakelock/wakelock.dart';

class VlcPlayerWidget extends StatefulWidget {
  String title;
  String url;

  VlcPlayerWidget({Key key, this.title, @required this.url}) : super(key: key);

  @override
  _VlcPlayerState createState() => _VlcPlayerState();
}

class _VlcPlayerState extends State<VlcPlayerWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  Uint8List image;

  // The following lines of code toggle the wakelock based on a bool value.
  bool enable = true;

// The following statement enables the wakelock.
  double _sliderValue = 20;
  VlcPlayerController _videoViewController;
  bool isPlaying = true;
  double sliderValue = 0.0;
  double currentPlayerTime = 0;
  double volumeValue = 100;
  var _focusNode = FocusNode();
  bool show = false;
  bool _isVisible = false;
  bool onBuffer = false;
  bool onError = false;
  Timer _timer;
  var pTimer;

  String token;
  bool getCastDeviceBtnEnabled = false;

  Future<void> initializePlayer() async {}

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    super.initState();
    Wakelock.toggle(enable: enable);

    _videoViewController = VlcPlayerController.network(
      widget.url,
      hwAcc: HwAcc.FULL,
      onInit: () {
        _startPLayerTimer();
        _startCastDiscover();
      },
      autoPlay: true,
      options: VlcPlayerOptions(),
    );
    prefs.then((value) {
      token = value.getString(Constants.USER_TOKEN);
      checkFavouriteBloc..getCheckFavourite(token, widget.title);
    }).catchError((onError) {
      Constants.showToast(context, "You need to login to perform this action.");
    });
  }

  @override
  void dispose() async {
    super.dispose();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    await Wakelock.toggle(enable: true);
    pTimer.cancel();
    await _videoViewController.stop();
    await _videoViewController.dispose();
    await _videoViewController.stopRendererScanning();
    _updateSliderVisibility();
    checkFavouriteBloc..dispose();
  }

  void _initializeTimer() {
    if (_timer != null) {
      _timer.cancel();
    }
    _timer = Timer(const Duration(seconds: 10), _updateSliderVisibility);
  }

  void _updateSliderVisibility() {
    _timer?.cancel();
    _timer = null;
    if (this.mounted) {
      setState(() {
        _isVisible = false;
        show = false;
      });
    }
  }

  void onEventKey(RawKeyEvent event) async {
    if ((event.logicalKey.debugName.toString() == 'Enter' ||
            event.logicalKey.debugName.toString() == 'Select') &&
        _videoViewController.value.duration.inSeconds > 0) {
      _initializeTimer();
      setState(() {
        _isVisible = true;
      });
    }
  }

  String _printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  String seconds2Minutes(seconds) {
    double minutes = (seconds / 60);
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    return minutes.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Shortcuts(
      shortcuts: <LogicalKeySet, Intent>{
        LogicalKeySet(LogicalKeyboardKey.select): ActivateIntent(),
      },
      child: GestureDetector(
        onTap: () {
          _initializeTimer();
          if (_videoViewController.value.duration.inSeconds > 0) {
            setState(() {
              _focusNode.requestFocus();
              _isVisible = true;
              show = true;
            });
          } else {
            setState(() {
              show = false;
              _isVisible = true;
            });
          }
        },
        child: new Scaffold(
          key: _scaffoldKey,
          body: Builder(builder: (context) {
            return SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Center(
                child: Stack(
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: new VlcPlayer(
                        aspectRatio: 16 / 9,
                        controller: _videoViewController,
                      ),
                    ),
                    //Container

                    Visibility(
                      visible: show,
                      child: Positioned(
                        bottom: 30,
                        height: 20,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.white,
                          child: Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Row(
                              children: <Widget>[
                                Text(
                                    _printDuration(
                                        Duration(seconds: sliderValue.toInt())),
                                    style: TextStyle(
                                      color: Style.Colors.primaryColor,
                                      // color: Color.fromRGBO(102, 45, 152, 1),
                                    )),
                                Text("/",
                                    style: TextStyle(
                                      color: Style.Colors.primaryColor,
                                      // color: Color.fromRGBO(102, 45, 152, 1),
                                    )),
                                Text(
                                    _printDuration(Duration(
                                        seconds: _videoViewController
                                            .value.duration.inSeconds)),
                                    style: TextStyle(
                                      color: Style.Colors.primaryColor,
                                    )),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),

                    Visibility(
                      visible: show,
                      child: Positioned(
                        bottom: 0,
                        height: 30,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.white,
                          child: Slider(
                            focusNode: _focusNode,
                            activeColor: Style.Colors.primaryColor,
                            // activeColor: Color.fromRGBO(102, 45, 152, 1),
                            value: sliderValue,
                            min: 0.0,
                            max: _videoViewController.value.duration == null
                                ? 1.0
                                : _videoViewController.value.duration.inSeconds
                                    .toDouble(),
                            onChanged: (progress) {
                              setState(() {
                                sliderValue = progress.floor().toDouble();
                              });
                              //convert to Milliseconds since VLC requires MS to set time
                              _videoViewController
                                  .setTime(sliderValue.toInt() * 1000);
                            },
                          ),
                        ),
                      ),
                    ), //Container

                    Visibility(
                      visible: _isVisible,
                      child: Positioned(
                        top: 0,
                        height: 50,
                        child: Container(
                          height: 75,
                          width: MediaQuery.of(context).size.width,
                          color: Style.Colors.primaryColor,
                          child: Row(
                            children: [
                              Expanded(
                                child: Row(
                                  children: [
                                    IconButton(
                                        icon: Icon(
                                          Icons.arrow_back,
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        }),
                                    Text(
                                      widget.title,
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.orange, // background
                                  onPrimary: Colors.white, // foreground
                                ),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.cast,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      "Cast",
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                                onPressed: !getCastDeviceBtnEnabled
                                    ? null
                                    : () {
                                        _getRendererDevices();
                                      },
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              StreamBuilder<CheckFavouriteResponse>(
                                stream: checkFavouriteBloc.subject.stream,
                                builder: (context,
                                    AsyncSnapshot<CheckFavouriteResponse>
                                        snapshot) {
                                  if (snapshot.hasData) {
                                    if (snapshot.data.error != null &&
                                        snapshot.data.error.length > 0) {
                                      return Container();
                                    }
                                    if (snapshot.data.data.isFavorite) {
                                      return RaisedButton(
                                        color: Colors.orange,
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.favorite_outlined,
                                              color: Colors.white,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                              "Remove from Favourites",
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ],
                                        ),
                                        onPressed: () {
                                          checkFavouriteBloc..drainStream();
                                          removeFromFavourite();
                                        },
                                      );
                                    } else {
                                      return RaisedButton(
                                        color: Colors.orange,
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.favorite_outline,
                                              color: Colors.white,
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                              "Add to Favourites",
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ],
                                        ),
                                        onPressed: () {
                                          channelsBloc.drainStream();
                                          checkFavouriteBloc..drainStream();
                                          addToFavourite();
                                        },
                                      );
                                    }
                                  } else if (snapshot.hasError) {
                                    return Container();
                                  } else {
                                    return CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.orange),
                                    );
                                  }
                                },
                              ),
                              SizedBox(
                                width: 20,
                              ),
                            ],
                          ),
                        ),
                      ), //Container
                    ),

                    Visibility(
                      visible: onBuffer,
                      child: Center(
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.orange),
                        ),
                      ),
                    ),
                  ], //<Widget>[]
                ), //Stack
              ), //Center
            );
          }),
        ),
      ),
    );
  }

  void addToFavourite() {
    final Repositories _repositories = Repositories();
    _repositories.addFavourite(token, widget.title).then((data) {
      if (data.data.status == "success") {
        checkFavouriteBloc..getCheckFavourite(token, widget.title);
        refreshHomeList();
        Constants.showToast(context, "Added to Favourite Successfully!");
      } else {
        Constants.showToast(context, "Adding to Favourite Failed!");
      }
    }).catchError((onError) {
      Constants.showToast(context, onError);
    });
  }

  void removeFromFavourite() {
    final Repositories _repositories = Repositories();
    _repositories.removeFavourite(token, widget.title).then((data) {
      if (data.data.status == "success") {
        checkFavouriteBloc..drainStream();
        checkFavouriteBloc..getCheckFavourite(token, widget.title);
        refreshHomeList();
        Constants.showToast(context, "Removed from Favourite Successfully!");
      } else {
        Constants.showToast(context, "Removed from Favourite Failed!");
      }
    }).catchError((onError) {
      Constants.showToast(context, onError);
    });
  }

  void refreshHomeList() {
    if (token == null) {
      categoriesBloc..getCategories(token);
    }
  }

  void _startCastDiscover() async {
    await _videoViewController.startRendererScanning();
    setState(() {
      getCastDeviceBtnEnabled = true;
    });
  }

  void _getRendererDevices() async {
    var castDevices = await _videoViewController.getRendererDevices();
    //
    if (castDevices != null && castDevices.isNotEmpty) {
      var selectedCastDeviceName = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Display Devices'),
            content: Container(
              width: double.maxFinite,
              height: 250,
              child: ListView.builder(
                itemCount: castDevices.keys.length + 1,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(
                      index < castDevices.keys.length
                          ? castDevices.values.elementAt(index).toString()
                          : 'Disconnect',
                    ),
                    onTap: () {
                      Navigator.pop(
                        context,
                        index < castDevices.keys.length
                            ? castDevices.keys.elementAt(index)
                            : null,
                      );
                    },
                  );
                },
              ),
            ),
          );
        },
      );
      await _videoViewController.castToRenderer(selectedCastDeviceName);
    } else {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('No Display Device Found!')));
    }
  }

  void _startPLayerTimer() {
    pTimer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      if (this.mounted) {
        String state = _videoViewController.value.playingState.toString();
        setState(() {
          if (state == "PlayingState.playing" &&
              sliderValue < _videoViewController.value.duration.inSeconds) {
            sliderValue =
                _videoViewController.value.position.inSeconds.toDouble();
          }
          switch (_videoViewController.value.playingState.toString()) {
            case "PlayingState.playing":
              onError = false;
              onBuffer = false;
              break;
            case "PlayingState.paused":
              onError = false;
              onBuffer = false;
              break;
            case "PlayingState.stopped":
              onError = false;
              onBuffer = false;
              break;
            case "PlayingState.buffering":
              onError = false;
              onBuffer = true;
              break;
            case "PlayingState.error":
              onError = true;
              onBuffer = false;
              break;
            default:
              onError = false;
              onBuffer = true;
              break;
          }
        });
      }
    });
  }
}
