// import 'package:flutter/material.dart';
// import 'package:flutter_vlc_player/flutter_vlc_player.dart';
// import 'package:mobile/theme/Colors.dart' as Style;
//
// class VideoDisplay extends StatefulWidget {
//   final String title;
//   final String url;
//   VideoDisplay({this.title, @required this.url});
//   @override
//   _VideoDisplayState createState() => _VideoDisplayState();
// }
//
// class _VideoDisplayState extends State<VideoDisplay> {
//   VlcPlayerController _videoPlayerController;
//
//   @override
//   void initState() {
//     _videoPlayerController = VlcPlayerController.network(
//       widget.url,
//       hwAcc: HwAcc.FULL,
//       autoPlay: true,
//       onInit: () {
//         _videoPlayerController.setLooping(true);
//         _videoPlayerController.startRendererScanning();
//       },
//       options: VlcPlayerOptions(),
//       autoInitialize: true,
//     );
//     _videoPlayerController.addListener(() {});
//     super.initState();
//   }
//
//   @override
//   void dispose() async {
//     super.dispose();
//     await _videoPlayerController.stopRendererScanning();
//     await _videoPlayerController.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Style.Colors.primaryColor,
//         title: Text(widget.title),
//       ),
//       body: Center(
//         child: VlcPlayer(
//           controller: _videoPlayerController,
//           aspectRatio: 16 / 9,
//           placeholder: Center(child: CircularProgressIndicator()),
//         ),
//       ),
//     );
//   }
//   // @override
//   // Widget build(BuildContext context) {
//   //   return VlcPlayerWidget(
//   //     title: widget.title,
//   //     url: widget.url,
//   //   );
//   // }
// }
