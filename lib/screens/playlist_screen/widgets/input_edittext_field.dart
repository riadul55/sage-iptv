import 'package:flutter/material.dart';
import 'package:mobile/screens/auth_screen/widgets/text_field_container.dart';
import 'package:mobile/theme/Colors.dart' as Style;

class InputEdittextField extends StatelessWidget {
  final String hintText;
  final String text;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const InputEdittextField({
    Key key,
    this.hintText,
    this.text,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        onChanged: onChanged,
        cursorColor: Style.Colors.secondaryColor,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: Colors.white,
          ),
          hintText: hintText,
          hintStyle: TextStyle(color: Style.Colors.secondaryColor),
          border: InputBorder.none,
        ),
        style: TextStyle(
          color: Colors.white,
        ),
        controller: text != null
            ? TextEditingController(text: text)
            : TextEditingController(),
      ),
    );
  }
}
