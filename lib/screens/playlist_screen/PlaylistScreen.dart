import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mobile/api_bloc/get_categories_bloc.dart';
import 'package:mobile/api_bloc/get_playlist_bloc.dart';
import 'package:mobile/local_database_guest_user/injection_container.dart';
import 'package:mobile/models/playlist/playlist_response.dart';
import 'package:mobile/models/playlist/update_response.dart';
import 'package:mobile/repositories/DBRepositories.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/theme/Colors.dart' as Style;
import 'package:mobile/utilities/PermissionUtil.dart';
import 'package:mobile/utilities/StringConstants.dart';
import 'package:mobile/utilities/constants.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'AddPlaylistScreen.dart';
import 'widgets/input_edittext_field.dart';

class PlaylistScreen extends StatefulWidget {
  final BuildContext context;

  PlaylistScreen({Key key, this.context}) : super(key: key);

  @override
  _PlaylistScreenState createState() => _PlaylistScreenState();
}

class _PlaylistScreenState extends State<PlaylistScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  String token;
  String playlistName, playlistUrl;
  PlayListBloc playListBloc;
  final Repositories _repositories = Repositories();

  @override
  void initState() {
    super.initState();
    playListBloc = getPlaylistBloc;

    prefs.then((value) {
      token = value.getString(Constants.USER_TOKEN);
      fetchData();
    }).catchError((onError) {
      Constants.showToast(context, "You need to login to perform this action.");
    });
  }

  void fetchData() {
    playListBloc.getPlayList(token);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    playListBloc.drainStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Style.Colors.primaryColor,
        centerTitle: true,
        title: Text("Manage Playlists"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              _showAddPlaylistDialog();
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showAddPlaylistDialog();
        },
        backgroundColor: Style.Colors.primaryColor,
        child: Icon(
          Icons.add,
        ),
      ),
      body: StreamBuilder<PlayListResponse>(
        stream: playListBloc.subject.stream,
        builder: (context, AsyncSnapshot<PlayListResponse> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return Container();
            }
            return ListView.builder(
              itemCount: snapshot.data.playList.lists.length,
              itemBuilder: (context, index) {
                var data = snapshot.data.playList.lists[index];
                return Card(
                  margin: EdgeInsets.only(left: 10, right: 10, top: 20),
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: ListTile(
                    leading: Icon(Icons.list),
                    title: Text(
                      data.listName,
                      style: TextStyle(
                        color: Style.Colors.primaryColor,
                        fontSize: 16,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                    onTap: () {
                      _showOptionsPlaylistDialog(data);
                    },
                  ),
                );
              },
            );
          } else if (snapshot.hasError) {
            return Container();
          } else {
            return _buildLoadingWidget();
          }
        },
      ),
    );
  }

  Future<void> _showAddPlaylistDialog({PlayListModel list}) async {
    if (token == null) {
      return Navigator.of(context)
          .push(
        MaterialPageRoute(
          builder: (BuildContext context) => AddPlaylistScreen(
            isUpdate: list != null,
            model: list,
            isfromHome: false,
          ),
        ),
      )
          .then((_) {
        fetchData();
      });
    }
    if (list != null) {
      this.playlistName = list.listName;
      this.playlistUrl = list.listUrl;
    }
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Style.Colors.primaryColor,
          title: Text(
            list != null ? 'Update Playlist' : 'Add Playlist',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                InputEdittextField(
                  hintText: "Playlist name",
                  text: list != null ? list.listName : "",
                  icon: Icons.edit,
                  onChanged: (value) {
                    this.playlistName = value;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                InputEdittextField(
                  hintText: "Playlist url",
                  text: list != null ? list.listUrl : "",
                  icon: Icons.link,
                  onChanged: (value) {
                    this.playlistUrl = value;
                  },
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Close',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(
                'Save',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                if (list != null) {
                  _editPlayList(list);
                } else {
                  _addPlayList();
                }
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showDeleteWarningDialog(PlayListModel list) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Warning'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you sure?'),
                Text('You want to delete ' + list.listName),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Yes'),
              onPressed: () {
                _deletePlayList(list);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showOptionsPlaylistDialog(PlayListModel list) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          // backgroundColor: Style.Colors.primaryColor,
          title: Text(
            list.listName,
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.orange, // background
                    onPrimary: Colors.white, // foreground
                  ),
                  child: Text("EDIT"),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _showAddPlaylistDialog(list: list);
                  },
                ),
                SizedBox(
                  height: 16,
                ),
                if (list.listUrl.startsWith("https://") ||
                    list.listUrl.startsWith("http://")) ...[
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.orange, // background
                      onPrimary: Colors.white, // foreground
                    ),
                    child: Text("REFRESH"),
                    onPressed: () async {
                      Navigator.of(context).pop();

                      if (token == null) {
                        if (await PermissionUtil.getPermissionStatus(
                            Permission.storage)) {
                          refreshPlaylist(list);
                        } else {
                          bool result =
                              await PermissionUtil.getStoragePermission(
                                  context: context);
                          if (result) {
                            refreshPlaylist(list);
                          }
                        }
                      } else {
                        UpdateResponse response = await _repositories
                            .getPlaylistRefresh(token, list.listId);
                        if (response != null) {
                          if (response.error != null &&
                              response.error.length > 0) {
                            Constants.showToast(context,
                                "Something went wrong, please try again later");
                          } else {
                            refreshSuccessToast(context);
                          }
                        } else {
                          Constants.showToast(context,
                              "Something went wrong, please try again later");
                        }
                      }

                      // getPlaylistBloc..drainStream();
                      // getPlaylistBloc.getPlayList(token);
                      // _onHideLoading();
                    },
                  )
                ],
                SizedBox(
                  height: 16,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.orange, // background
                    onPrimary: Colors.white, // foreground
                  ),
                  child: Text("DELETE"),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _showDeleteWarningDialog(list);
                  },
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Close',
                style: TextStyle(
                  color: Style.Colors.primaryColor,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void refreshSuccessToast(BuildContext context) {
    Constants.showToast(context, StringConstants.REFRESH_SUCCESS);
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(Style.Colors.primaryColor),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }

  void _addPlayList() async {
    if (playlistName.isEmpty) {
      Constants.showToast(context, StringConstants.ERROR_NAME_EMPTY);
    } else if (playlistUrl.isEmpty) {
      Constants.showToast(context, StringConstants.ERROR_URL_EMPTY);
    } else {
      _onLoading();
      _repositories
          .getPlaylistAdd(token, playlistName, playlistUrl)
          .then((value) {
        if (value != null &&
            value.update != null &&
            value.update.status != null &&
            value.update.status == "success") {
          fetchData();
          Navigator.of(context).pop();
          Constants.showToast(context, StringConstants.ADD_SUCCESS);
          setState(() {});
          _onHideLoading();
        } else {
          Constants.showToast(
              context, "Something went wrong, Please try again!");
          _onHideLoading();
        }
      }).catchError((onError) {
        Constants.showToast(context, onError.toString());
        _onHideLoading();
      });
    }
  }

  void _editPlayList(PlayListModel list) async {
    if (playlistName.isEmpty) {
      Constants.showToast(context, "Name field is required!");
    } else if (playlistUrl.isEmpty) {
      Constants.showToast(context, "Url field is required!");
    } else {
      _onLoading();
      Repositories repositories = new Repositories();
      repositories
          .getPlaylistEdit(token, list.listId, playlistName, playlistUrl)
          .then((value) {
        if (value != null &&
            value.update != null &&
            value.update.status != null &&
            value.update.status == "success") {
          fetchData();
          Navigator.of(context).pop();
          Constants.showToast(context, "Playlist update successfully!");
          setState(() {});
          _onHideLoading();
        } else {
          Constants.showToast(
              context, "Something went wrong, Please try again!");
          _onHideLoading();
        }
      }).catchError((onError) {
        Constants.showToast(context, onError.toString());
        _onHideLoading();
      });
    }
  }

  void _deletePlayList(PlayListModel list) async {
    _onLoading();
    Repositories repositories = new Repositories();
    repositories.getPlaylistDelete(token, list.listId).then((value) async {
      if (value != null &&
          value.update != null &&
          value.update.status != null &&
          value.update.status == "success") {
        bool isListEmpty = await playListBloc.getPlayList(token);
        Navigator.of(context).pop();
        if (token == null && isListEmpty) {
          _onHideLoading();
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => AddPlaylistScreen(
                  model: list,
                  isfromHome: true,
                ),
              ),
              ModalRoute.withName("/"));
        } else {
          Constants.showToast(context, "Playlist delete successfully!");
          setState(() {});
          _onHideLoading();
        }
      } else {
        Constants.showToast(context, "Something went wrong, Please try again!");
        _onHideLoading();
      }
    }).catchError((onError) {
      Constants.showToast(context, onError.toString());
      _onHideLoading();
    });
  }

  void _onLoading() {
    showDialog(
      context: _scaffoldKey.currentState.context,
      barrierDismissible: false,
      builder: (_) => new SimpleDialog(
        title: Container(
          child: Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 10.0),
                  child: new Text("Loading"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onHideLoading() {
    Navigator.pop(_scaffoldKey.currentState.context);
  }

  Future<void> refreshPlaylist(PlayListModel playListModel) async {
    String path = "";
    EasyLoading.show(status: 'loading...');
    try {
      path = await _repositories.saveLocalPlayList(playListModel.listUrl);
      if (path.isNotEmpty) {
        await _repositories
            .deletePlayList(int.parse(playListModel.listId))
            .then(
          (value) {
            ComputeClassForRefreshList classForRefreshList =
                ComputeClassForRefreshList(
                    filePath: path,
                    playListName: playListModel.listName,
                    playListUrl: playListModel.listUrl);
            return _repositories
                .insertPlayListDataIntoDBCompute(classForRefreshList)
                .then(
              (value) async {
                print(" repositories::::: playlist insertion done");

                await EasyLoading.dismiss();
                // we are refreshing data for guest users only !!!

                refreshSuccessToast(context);
                fetchData();
                categoriesBloc..getCategories(token);
              },
            );
          },
        );
      } else {
        Constants.showToast(context, StringConstants.ERROR_DOWNLOADING);
      }
    } catch (e) {
      DBRepositories localDB = sl();
      await localDB.deletePlayLists(await localDB.fetchLastPlayListIt());
      await EasyLoading.dismiss();
      Constants.showToast(context, StringConstants.ERROR_DOWNLOADING);
    }
  }

  void hideDIalog() {
    _onHideLoading();
  }
}

// GridView.count(
// crossAxisCount: 2,
// children: List.generate(
// snapshot.data.playList.lists.length,
// (index) {
// var snapData = snapshot.data.playList.lists[index];
// return GestureDetector(
// // onLongPress: () {
// // },
// onTap: () {
// // Navigator.of(context).push(
// //   MaterialPageRoute(
// //     builder: (BuildContext context) {
// //       return VlcPlayerWidget(
// //         title: snapData.listName,
// //         url: snapData.listUrl,
// //       );
// //     },
// //   ),
// // );
// _showOptionsPlaylistDialog(
// snapshot.data.playList.lists[index]);
// },
// child: Padding(
// padding: EdgeInsets.only(
// left: 10,
// top: 10,
// right: 10,
// ),
// child: Card(
// elevation: 8,
// child: Column(
// mainAxisAlignment: MainAxisAlignment.center,
// children: [
// Icon(
// Icons.play_circle_outline,
// color: Style.Colors.primaryColor,
// size: 100,
// ),
// SizedBox(
// height: 20,
// ),
// Text(
// Uri.decodeComponent(snapData.listName),
// style: TextStyle(
// color: Style.Colors.primaryColor,
// ),
// ),
// ],
// ),
// ),
// ),
// );
// },
// ),
// );
