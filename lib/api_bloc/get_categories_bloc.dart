import 'package:flutter/material.dart';
import 'package:mobile/models/categories/category_response.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:rxdart/rxdart.dart';

class CategoriesBloc {
  final Repositories _repositories = Repositories();

  final BehaviorSubject<CategoryResponse> _subject =
      BehaviorSubject<CategoryResponse>();

  getCategories(String token) async {
    CategoryResponse response;

    response = await _repositories.getCategories(token);

    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<CategoryResponse> get subject => _subject;
}

final categoriesBloc = CategoriesBloc();
