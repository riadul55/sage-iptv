import 'package:flutter/material.dart';
import 'package:mobile/models/favourites/check_favourite_response.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:rxdart/rxdart.dart';

class CheckFavouriteBloc {
  final Repositories _repositories = Repositories();
  final BehaviorSubject<CheckFavouriteResponse> _subject =
  BehaviorSubject<CheckFavouriteResponse>();

  getCheckFavourite(String token, String title) async {
    CheckFavouriteResponse response = await _repositories.checkFavourite(token, title);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<CheckFavouriteResponse> get subject => _subject;
}

final checkFavouriteBloc = CheckFavouriteBloc();