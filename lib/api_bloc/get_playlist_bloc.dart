import 'package:flutter/material.dart';
import 'package:mobile/models/playlist/playlist_response.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:rxdart/rxdart.dart';

class PlayListBloc {
  final Repositories _repositories = Repositories();
  final BehaviorSubject<PlayListResponse> _subject =
      BehaviorSubject<PlayListResponse>();

  Future<bool> getPlayList(String token) async {
    PlayListResponse response = await _repositories.getPlaylist(token);
    _subject.sink.add(response);
    return response.playList.lists.isEmpty;
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<PlayListResponse> get subject => _subject;
}

final getPlaylistBloc = PlayListBloc();
