import 'package:flutter/material.dart';
import 'package:mobile/models/channels/channel_response.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:rxdart/rxdart.dart';

class ChannelsBloc {
  final Repositories _repositories = Repositories();
  final BehaviorSubject<ChannelResponse> _subject =
      BehaviorSubject<ChannelResponse>();

  getChannels(String token, String catId) async {
    ChannelResponse response = await _repositories.getChannels(token, catId);
    _subject.sink.add(response);
  }

  getChannelsBySearch(String token, String keyword) async {
    ChannelResponse response =
        await _repositories.getChannelsBySearch(token, keyword);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<ChannelResponse> get subject => _subject;
}

final channelsBloc = ChannelsBloc();
