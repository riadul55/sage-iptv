import 'package:flutter/material.dart';
import 'package:mobile/models/playlist/update_response.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:rxdart/rxdart.dart';

class UpdatePlayListBloc {
  final Repositories _repositories = Repositories();
  final BehaviorSubject<UpdateResponse> _subject =
      BehaviorSubject<UpdateResponse>();

  addPlayList(String token, String name, String url) async {
    UpdateResponse response =
        await _repositories.getPlaylistAdd(token, name, url);
    _subject.sink.add(response);
  }

  editPlayList(String token, String id, String name, String url) async {
    UpdateResponse response =
        await _repositories.getPlaylistEdit(token, id, name, url);
    _subject.sink.add(response);
  }

  deletePlayList(String token, String id) async {
    UpdateResponse response = await _repositories.getPlaylistDelete(token, id);
    _subject.sink.add(response);
  }

  refreshPlayList(String token, String id) async {
    UpdateResponse response = await _repositories.getPlaylistRefresh(token, id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<UpdateResponse> get subject => _subject;
}

final updatePlaylistBloc = UpdatePlayListBloc();
