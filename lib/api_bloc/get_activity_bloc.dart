import 'package:flutter/material.dart';
import 'package:mobile/models/activity/activity.dart';
import 'package:mobile/models/activity/activity_response.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:rxdart/rxdart.dart';

class ActivityBloc {
  final Repositories _repositories = Repositories();
  final BehaviorSubject<ActivityResponse> _subject =
      BehaviorSubject<ActivityResponse>();

  getActivity(String token) async {
    if (token == null || token.isEmpty) {
      Activity activity = Activity(
        showAds: true,
      );
      ActivityResponse response =
          ActivityResponse(activity, null, isGuestUser: true);
      _subject.sink.add(response);
    } else {
      ActivityResponse response = await _repositories.getActivity(token);
      _subject.sink.add(response);
    }
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<ActivityResponse> get subject => _subject;
}

final activityBloc = ActivityBloc();
