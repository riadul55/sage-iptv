import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class Constants {
  static const String LOGO_NAME = 'assets/images/sageiptv.png';

  // static const String IS_LOGED_IN = "isLogin";
  static const String USER_TOKEN = "userToken";
  static const String SUCCESS = "success";
  static const String USER_EMAIL = "userEmail";
  static const String IS_LIST_GRID = "isListGrid";

  static const String ANDROID_PRODUCT_ID = "sageiptv.android.premium.access";
  static const String IOS_PRODUCT_ID = "sageiptv.ios.go.premium.access";

  static const String ANDROID_REMOVE_ADS = "sageiptv.remove.ads.android";
  static const String IOS_REMOVE_ADS = "sageiptv.remove.ads.ios";

  static const String GO_PREMIUM_DESC = "<ul><li>No more Ads</li><li>Manage unlimited playlists</li><li>Free Lifetime Premium Access for our TV apps when you login with the same account</li><li>Free support by email (Same day)</li></ul>";
  static const String REMOVE_ADS_DESC = "<ul><li>No more Ads</li><li>Free support by email</li></ul>";

  static const String CHANNEL_NAME = "tvg-name";
  static const String CHANNEL_LOGO = "tvg-logo";
  static const String GROUP_TITLE = "group-title";

  static Future<Void> showToastF(BuildContext context, String msg,
      {int duration, int gravity}) {
    FocusScope.of(context).requestFocus(FocusNode());

    Toast.show(msg, context,
        duration: duration != null ? duration : Toast.LENGTH_LONG,
        gravity: gravity != null ? gravity : Toast.BOTTOM);
  }

  static void showToast(BuildContext context, String msg,
      {int duration, int gravity}) {
    FocusScope.of(context).requestFocus(FocusNode());

    Toast.show(msg, context,
        duration: duration != null ? duration : Toast.LENGTH_LONG,
        gravity: gravity != null ? gravity : Toast.BOTTOM);
  }
}
