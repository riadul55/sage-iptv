import 'dart:convert';

import 'package:mobile/local_database_guest_user/AppDb.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/utilities/constants.dart';

import 'exportParser.dart';

/// A parser of M3U documents.
///
/// At the moment the parsing if done thought the static method [parse].
///
/// The parsing is done line by line. There are some context aware variables
/// to know the state of the current entry [_currentInfoEntry]
/// and what type of line should come next [_nextLineExpected].
class M3uParser {
  /// Parse a document represented by the [source]
  ///
  /// [source] a string value of the full document.
  static Future<DBListData> parse(
          String source, PlaylistTable playlistTable) async =>
      M3uParser()._parse(source, playlistTable);

  /// Internally used after the header is parsed.
  FileTypeHeader _fileType;

  /// Controller for the current state of the parser
  /// This flag indicates the next type of data that we expect.
  LineParsedType _nextLineExpected = LineParsedType.header;

  /// Current holder of the information about the current Track
  EntryInformation _currentInfoEntry;

  /// Result accumulator of the parser.
  final List<ChannelsTable> _channels = [];

  List<CategoriesTable> _categories = [];

  PlaylistTable _playlistTable;
  Map<String, int> mapOfCategories = {};

  /// Main parse function
  ///
  /// Splits the file into lines and parse line by line.
  ///
  /// [source] source file to parse.
  ///
  /// Can [throws] [InvalidFormatException] if the file is not supported.
  Future<DBListData> _parse(String source, PlaylistTable playlistTable) async {
    _playlistTable = playlistTable;

    LineSplitter.split(source).forEach(_parseLine);

    _categories = mapOfCategories.entries
        .map((entry) => CategoriesTable(
            counter: entry.value,
            playListId: _playlistTable.id,
            id: entry.key + _playlistTable.id.toString(),
            name: entry.key))
        .toList();

    return DBListData(categories: _categories, channels: _channels);
  }

  void _parseLine(String line) {
    switch (_nextLineExpected) {
      case LineParsedType.header:
        _fileType = FileTypeHeader.fromString(line);
        _nextLineExpected = LineParsedType.info;
        break;
      case LineParsedType.info:
        final parsedEntry = _parseInfoRow(line, _fileType);
        if (parsedEntry == null) {
          break;
        }
        _currentInfoEntry = parsedEntry;
        _nextLineExpected = LineParsedType.source;
        break;
      case LineParsedType.source:
        if (_currentInfoEntry == null) {
          _nextLineExpected = LineParsedType.info;
          _parseLine(line);
          break;
        }
        String categoryName = "others";

        if (_currentInfoEntry.attributes.containsKey(Constants.GROUP_TITLE)) {
          categoryName = _currentInfoEntry.attributes[Constants.GROUP_TITLE];
        }
        if (categoryName == null) {
          categoryName = "others";
        } else if (_currentInfoEntry.attributes.length < 1) {
          categoryName = _playlistTable.name;
        }

        if (!mapOfCategories.containsKey(categoryName)) {
          mapOfCategories.putIfAbsent(categoryName, () => 1);
        } else {
          int count = mapOfCategories[categoryName];
          mapOfCategories.update(categoryName, (value) => count + 1);
        }

        ChannelsTable channelsTable = ChannelsTable(
            channelId: getChannelName(),
            playListId: _playlistTable.id,
            categoryId: categoryName + _playlistTable.id.toString(),
            url: line,
            name: getChannelName(),
            logo: _currentInfoEntry.attributes[Constants.CHANNEL_LOGO] != null
                ? _currentInfoEntry.attributes[Constants.CHANNEL_LOGO]
                : "");
        _channels.add(channelsTable);

        // _playlist.add(M3uGenericEntry.fromEntryInformation(
        //     information: _currentInfoEntry, link: line));
        _currentInfoEntry = null;
        _nextLineExpected = LineParsedType.info;
        break;
    }
  }

  String getChannelName() {
    return _currentInfoEntry.attributes[Constants.CHANNEL_NAME] != null
        ? _currentInfoEntry.attributes[Constants.CHANNEL_NAME]
        : _currentInfoEntry.title;
  }

  EntryInformation _parseInfoRow(String line, FileTypeHeader fileType) {
    switch (fileType) {
      case FileTypeHeader.m3u:
        return _regexParse(line);
      case FileTypeHeader.m3uPlus:
        return _regexParse(line);
      default:
        throw InvalidFormatException(InvalidFormatType.other,
            originalValue: line);
    }
  }

  /// This parses the metadata information of a line.
  /// This is a Regex parser caution is advised.
  EntryInformation _regexParse(String line) {
    final regexExpression = RegExp(r' (.*?)=\"(.*?)"|,(.*)');
    final matches = regexExpression.allMatches(line);
    final attributes = <String, String>{};
    var title = '';

    matches.forEach((match) {
      if (match[1] != null && match[2] != null) {
        attributes[match[1]] = match[2];
      } else if (match[3] != null) {
        title = match[3];
      } else {
        print('ERROR regexing against -> ${match[0]}');
      }
    });
    return EntryInformation(title: title, attributes: attributes);
  }
}
