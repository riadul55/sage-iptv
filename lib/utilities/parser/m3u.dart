import 'package:mobile/local_database_guest_user/AppDb.dart';
import 'package:mobile/repositories/Repositories.dart';
import 'package:mobile/utilities/parser/m3u_parser.dart';

/// Parse a document represented by the [source]
///
/// [source] a string value of the full document.
Future<DBListData> parseFile(
        String source, PlaylistTable playlistTable) async =>
    M3uParser.parse(source, playlistTable);

// // /// From a list of categories return them organized by a property
// // /// [entries] all the entries from the response
// // /// [attributeName] name of the attribute to merge for
// // /// [defaultAttribute] when the attribute is not found category
// // /// to merge properties.
// Map<String, List<M3uGenericEntry>> sortedCategories(
//         {@required List<M3uGenericEntry> entries,
//         @required String attributeName,
//         String defaultAttribute = 'other'}) =>
//     PlaylistHelper.sortedCategories(
//         entries: entries,
//         attributeName: attributeName,
//         defaultAttribute: defaultAttribute);
