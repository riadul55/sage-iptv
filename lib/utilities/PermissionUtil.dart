import 'dart:io';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionUtil {
  static Future<bool> getStoragePermission({BuildContext context}) async {
    PermissionStatus status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      return true;
    } else {
      if (Platform.isAndroid) {
        if (status == PermissionStatus.permanentlyDenied ||
            status.isRestricted) {
          await customDialog(
            context: context,
          );
          return false;
        } else {
          return false;
        }
      } else if (Platform.isIOS) {
        if (status == PermissionStatus.denied ||
            status == PermissionStatus.restricted) {
          await customDialog(
            context: context,
          );
          return false;
        } else {
          return false;
        }
      }
    }
    return false;
  }

  static Future<bool> getPermissionStatus(Permission permission) async {
    PermissionStatus status = await permission.status;
    return status == PermissionStatus.granted;
  }

  static customDialog({
    BuildContext context,
  }) =>
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Permission required'),
          content: Text(
              'Please provide file access permission from settings to save Playlist'),
          actions: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            TextButton(
              child: Text('Go to Settings'),
              onPressed: () async {
                await openAppSettings();
              },
            )
          ],
        ),
      );
}

showLoadingDialog(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (_) => new SimpleDialog(
      title: Container(
        child: Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new CircularProgressIndicator(),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 10.0),
                child: new Text("Loading"),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
