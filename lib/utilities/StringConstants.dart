class StringConstants {
  static const String ALREADY_HAVING_ACCOUNT =
      "Already have an account with us? Please, login here";
  static const String LABEL_ADD = "ADD";
  static const String LABEL_UPDATE = "UPDATE";
  static const String LABEL_PLAYLIST_URL = "Playlist url";
  static const String LABEL_PLAYLIST_NAME = "Playlist name";
  static const String ADD_PLAYLIST = "Add Playlist";
  static const String UPDATE_PLAYLIST = "Update Playlist";
  static const String FROM_FILE = "File";
  static const String SELECT_FILE = "Select File";
  static const String FROM_URL = "URL";
  static const String LABEL_PLAYLIST = "PlayList : ";
  static const String FROM_PORTAL = "Portal";
  static const String FROM_PORTAL_URL = "Portal Url";
  static const String FROM_PORTAL_PORT = "Portal Port";
  static const String FROM_PORTAL_USERNAME = "Portal Username";
  static const String FROM_PORTAL_PASSWORD = "Portal Password";

  static const String LABEL_LOGIN = "Log in";
  static const String LABEL_LOGOUT = "Log out";
  static const String ERROR_NAME_EMPTY = "Name field is required!";
  static const String ERROR_FIELD_EMPTY = "is required!";
  static const String ERROR_URL_EMPTY = "Url field is required!";
  static const String FAVOURITES = "~~~FAVOURITES~~~";
  static const String FAVOURITES_TITLE = "FAVOURITES";
  static const String CONTINUE_AS_GUEST = "Continue as Guest";
  static const String ERROR_DOWNLOADING =
      "Something went wrong, please try again";
  static const String REFRESH_SUCCESS = "Refreshed successfully";
  static const String ADD_SUCCESS = "Playlist added successfully!";
}
