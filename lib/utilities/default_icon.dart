import 'package:flutter/material.dart';

class DefaultIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Icon(
        Icons.ondemand_video_sharp,
        color: Colors.orange,
      );
}
