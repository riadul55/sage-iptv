class DbUtils {
  static final String DATABASE_NAME = '.db';
  static final int DATABASE_VERSION = 1;

  static final String TABLE_NAME = '';
  static final String TABLE_QUERY =
  '''CREATE TABLE $TABLE_NAME (  
        id INTEGER PRIMARY KEY, 
        content TEXT )''';
}
