import 'dart:io';
import 'dart:async';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:sqflite/sqflite.dart';

import 'db_utils.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initialDatabase();
    return _database;
  }

  initialDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    print(directory.path);
    String path = p.join(directory.path, DbUtils.DATABASE_NAME);
    return await openDatabase(
      path,
      version: DbUtils.DATABASE_VERSION,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) {
    db.execute(DbUtils.TABLE_QUERY);
  }


//---------------
  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(DbUtils.TABLE_NAME, row);
  }

  Future<List<Map<String, dynamic>>> queryAll() async {
    Database db = await instance.database;
    return await db.query(DbUtils.TABLE_NAME);
  }

  Future update(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int id = row['id'];
    return db.update(DbUtils.TABLE_NAME, row,
        where: 'id = ?', whereArgs: [id]);
  }

  Future<int> delete(int id) async {
    Database db = await instance.database;
    return await db
        .delete(DbUtils.TABLE_NAME, where: 'id = ?', whereArgs: [id]);
  }

  Future<int> deleteAll() async {
    Database db = await instance.database;
    return await db.delete(DbUtils.TABLE_NAME);
  }
}
