import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_stetho/flutter_stetho.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

import 'local_database_guest_user/injection_container.dart' as di;
import 'screens/splash_screen/SplashScreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  di.init();
  Admob.initialize();
  InAppPurchaseConnection.enablePendingPurchases();
  Stetho.initialize();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: EasyLoading.init(),
      debugShowCheckedModeBanner: false,
      title: 'SageIPTV',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
    );
  }
}
