class UpdateResponse {
  final Update update;
  final String error;

  UpdateResponse(this.update, this.error);

  UpdateResponse.fromJson(Map<String, dynamic> json)
      : update = new Update.fromJson(json),
        error = "";

  UpdateResponse.withError(String errorValue)
      : update = Update(),
        error = errorValue;
}

class Update {
  String status;

  Update({this.status});

  Update.fromJson(Map<String, dynamic> json) {
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    return data;
  }
}
