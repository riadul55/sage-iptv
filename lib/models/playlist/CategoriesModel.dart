import 'package:mobile/utilities/parser/generic_entry.dart';

class CategoriesModel {
  final String nameString;
  final List<M3uGenericEntry> list;

  CategoriesModel(this.nameString, this.list);
}
