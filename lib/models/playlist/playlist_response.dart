class PlayListResponse {
  final PlayList playList;
  final String error;

  PlayListResponse(this.playList, this.error);

  PlayListResponse.fromJson(Map<String, dynamic> json)
      : playList = new PlayList.fromJson(json),
        error = "";

  PlayListResponse.withError(String errorValue)
      : playList = PlayList(),
        error = errorValue;
}

class PlayList {
  List<PlayListModel> lists;

  PlayList({this.lists});

  PlayList.fromJson(Map<String, dynamic> json) {
    if (json['lists'] != null) {
      lists = <PlayListModel>[];
      json['lists'].forEach((v) {
        lists.add(new PlayListModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.lists != null) {
      data['lists'] = this.lists.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PlayListModel {
  String listId;
  String listName;
  String listUrl;

  PlayListModel({this.listId, this.listName, this.listUrl});

  PlayListModel.fromJson(Map<String, dynamic> json) {
    listId = json['list_id'];
    listName = json['list_name'];
    listUrl = json['list_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['list_id'] = this.listId;
    data['list_name'] = this.listName;
    data['list_url'] = this.listUrl;
    return data;
  }
}
