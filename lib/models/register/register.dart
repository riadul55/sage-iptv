class Register {
  String registration;
  String token;
  String reason;

  Register({this.registration, this.token, this.reason});

  Register.fromJson(Map<String, dynamic> json) {
    registration = json['registration'];
    token = json['token'];
    reason = json['reason'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['registration'] = this.registration;
    data['token'] = this.token;
    data['reason'] = this.reason;
    return data;
  }
}
