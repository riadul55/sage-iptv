
import 'register.dart';

class RegisterResponse {
  final Register register;
  final String error;

  RegisterResponse(this.register, this.error);

  RegisterResponse.fromJson(Map<String, dynamic> json)
      : register = new Register.fromJson(json),
        error = "";

  RegisterResponse.withError(String errorValue)
      : register = Register(),
        error = errorValue;
}
