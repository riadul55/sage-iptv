class Login {
  String loggedIn;
  bool isActivated;
  String token;

  Login({this.loggedIn, this.isActivated, this.token});

  Login.fromJson(Map<String, dynamic> json) {
    loggedIn = json['logged_in'];
    isActivated = json['isActivated'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['logged_in'] = this.loggedIn;
    data['isActivated'] = this.isActivated;
    data['token'] = this.token;
    return data;
  }
}
