
import 'channels.dart';

class ChannelResponse {
  final Channels channels;
  final String error;

  ChannelResponse(this.channels, this.error);

  ChannelResponse.fromJson(Map<String, dynamic> json)
      : channels = new Channels.fromJson(json),
        error = "";

  ChannelResponse.withError(String errorValue)
      : channels = Channels(),
        error = errorValue;
}
