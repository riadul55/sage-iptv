class Channels {
  List<ChannelModel> items;

  Channels({this.items});

  Channels.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = <ChannelModel>[];
      json['items'].forEach((v) {
        items.add(new ChannelModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ChannelModel {
  String id;
  String title;
  String url;
  String logo;
  String tvgId;

  ChannelModel({this.id, this.title, this.url, this.logo, this.tvgId});

  ChannelModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    url = json['url'];
    logo = json['logo'];
    tvgId = json['tvg_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['url'] = this.url;
    data['logo'] = this.logo;
    data['tvg_id'] = this.tvgId;
    return data;
  }
}
