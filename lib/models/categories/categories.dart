class Categories {
  String displayMode;
  String poster;
  List<CategoryListModel> list;

  Categories({this.displayMode, this.poster, this.list});

  Categories.fromJson(Map<String, dynamic> json) {
    displayMode = json['display_mode'];
    poster = json['poster'];
    if (json['list'] != null) {
      list = <CategoryListModel>[];
      json['list'].forEach((v) {
        list.add(new CategoryListModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['display_mode'] = this.displayMode;
    data['poster'] = this.poster;
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CategoryListModel {
  String id;
  String poster;
  String name;
  int counter;

  CategoryListModel({this.id, this.poster, this.name, this.counter});

  CategoryListModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    poster = json['poster'];
    name = json['name'];
    counter = json['counter'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['poster'] = this.poster;
    data['name'] = this.name;
    data['counter'] = this.counter;
    return data;
  }
}
