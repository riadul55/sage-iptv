
import 'categories.dart';

class CategoryResponse {
  final Categories categories;
  final String error;

  CategoryResponse(this.categories, this.error);

  CategoryResponse.fromJson(Map<String, dynamic> json)
      : categories = new Categories.fromJson(json),
        error = "";

  CategoryResponse.withError(String errorValue)
      : categories = Categories(),
        error = errorValue;
}
