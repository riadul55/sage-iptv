class CheckFavouriteResponse {
  final CheckFavourite data;
  final String error;

  CheckFavouriteResponse(this.data, this.error);

  CheckFavouriteResponse.fromJson(Map<String, dynamic> json)
      : data = new CheckFavourite.fromJson(json),
        error = "";

  CheckFavouriteResponse.withError(String errorValue)
      : data = CheckFavourite(),
        error = errorValue;
}

class CheckFavourite {
  bool isFavorite;

  CheckFavourite({this.isFavorite});

  CheckFavourite.fromJson(Map<String, dynamic> json) {
    isFavorite = json['is_favorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['is_favorite'] = this.isFavorite;
    return data;
  }
}