class StatusResponse {
  final Status data;
  final String error;

  StatusResponse(this.data, this.error);

  StatusResponse.fromJson(Map<String, dynamic> json)
      : data = new Status.fromJson(json),
        error = "";

  StatusResponse.withError(String errorValue)
      : data = Status(),
        error = errorValue;
}

class Status {
  String status;

  Status({this.status});

  Status.fromJson(Map<String, dynamic> json) {
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    return data;
  }
}
