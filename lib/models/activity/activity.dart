class Activity {
  bool isActivated;
  bool trial;
  String expirationDate;
  bool showAds;

  Activity(
      {this.isActivated, this.trial, this.expirationDate, this.showAds});

  Activity.fromJson(Map<String, dynamic> json) {
    isActivated = json['isActivated'];
    trial = json['trial'];
    expirationDate = json['expirationDate'];
    showAds = json['showAds'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isActivated'] = this.isActivated;
    data['trial'] = this.trial;
    data['expirationDate'] = this.expirationDate;
    data['showAds'] = this.showAds;
    return data;
  }
}
