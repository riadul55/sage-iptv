import 'activity.dart';

class ActivityResponse {
  final Activity activity;
  final String error;
  bool isGuestUser = false;

  ActivityResponse(this.activity, this.error, {this.isGuestUser = false});

  ActivityResponse.fromJson(Map<String, dynamic> json)
      : activity = new Activity.fromJson(json),
        error = "";

  ActivityResponse.withError(String errorValue)
      : activity = Activity(),
        error = errorValue;
}
